//
//  UtilityManager.swift
//  ConferenceSeries
//
//  Created by Manikumar on 18/01/18.
//  Copyright © 2018 Omics. All rights reserved.
//

import UIKit

final class UtilityManager: NSObject {
    
    private let dateService:DateUtility
    
    // Injecting Object
    init(dateService:DateUtility) {
        self.dateService = dateService
    }
    
    func getCurrentDate()->String{
        return dateService.currentDate
    }
    func getCurrentDate1()->Date{
        return dateService.currentDate1
    }
    func getMonth()->String{
        return dateService.currentMonth
    }
    func getWeek() -> String {
        return dateService.currentWeek
    }
    func getYear() -> String {
        return dateService.currentYear
    }
}
