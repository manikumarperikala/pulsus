//
//  filterModel.swift
//  ConferenceSeries
//
//  Created by Srinu Babu Gedela on 23/05/20.
//  Copyright © 2020 Omics. All rights reserved.
//

import Foundation


class Subjects
{
     var subject_id = String()
     var subject_name = String()
    var isSelected:Bool
    init(subject_id:String,subject_name:String,isSelected:Bool)
     {
        self.subject_id = subject_id
        self.subject_name = subject_name
        self.isSelected = isSelected
     }
}
class Tracks
{
    var track_id = String()
    var track_name = String()
    var track_desc = String()
    var sub_tracks = [String]()
    var isSelected:Bool
    init(track_id:String,track_name:String,track_desc:String,sub_tracks:[String],isSelected:Bool)
    {
        self.track_id = track_id
        self.track_name = track_name
        self.track_desc = track_desc
        self.sub_tracks = sub_tracks
        self.isSelected = isSelected
    }
}
class Months
{
     var month_id = String()
     var month_name = String()
     var isSelected:Bool
    init(month_id:String,month_name:String,isSelected:Bool)
     {
        self.month_id = month_id
        self.month_name = month_name
        self.isSelected = isSelected
     }
}

class Cities
{
    var city_id = String()
    var city_name = String()
    var isSelected = false
    init(city_id:String,city_name:String,isSelected:Bool)
    {
        self.city_id = city_id
        self.city_name = city_name
        self.isSelected = isSelected
    }
}

class Countries
{
     var country_id = String()
     var country_name = String()
     var cities = [Cities]()
     var isSelected:Bool
     var collapsed: Bool
    init(country_id:String,country_name:String,cities:[Cities],isSelected:Bool,collapsed:Bool)
     {
        self.country_id = country_id
        self.country_name = country_name
        self.cities = cities
        self.isSelected = isSelected
        self.collapsed = collapsed
     }
}

