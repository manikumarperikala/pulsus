//
//  ViewModel.swift
//  ConferenceSeries
//
//  Created by Manikumar on 16/02/18.
//  Copyright © 2018 Omics. All rights reserved.
//

/********************* Dependency Injection Implementation ********************/

import UIKit
import Alamofire
import SwiftyJSON


//MARK:- DateModel Protocol

protocol DateModel {
    
    var currentDate:String{get}
    var currentDate1:Date{get}
    var currentYear:String{get}
    var currentMonth:String{get}
    var currentWeek:String{get}
    
}

//MARK:- NetworkModel Protocol

protocol NetworkModel {
    
    // Post Request with 'String' Type parameters
    func postRequest(api:String,parameters:[String:String],completion:@escaping(_ data:Any?)->Void)
    // Put Request with 'String' Type parameters
    func putRequest(api:String,parameters:[String:String],completion:@escaping(_ data:Any?)->Void)
    
    // Post Request with 'Any' type parameters
    func postWithRequest(api:String,parameters:[String:Any],completion:@escaping(_ data:Any?)->Void)

    //Get request with 'String' type parmeters
    func getRequest(api:String,parameters:[String:String],completion:@escaping(_ data:Any?)->Void)
    
    func uploadfile(api: String, imageData: Data?, parameters: [String:Any],completion: @escaping((Any?) -> Void), onError: @escaping(Error?) -> Void)
    
    
    
    
}

//MARK:- Date Model Protocol Inheritance

final class DateUtility:DateModel{
    
    var currentDate: String{
        
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = kRequiredDateFormat
        return dateformatter.string(from:Date())
    }
    var currentDate1: Date
    {
       let dateformatter = DateFormatter()
       dateformatter.dateFormat = kRequiredDateFormat
        let curr = "\(Date())"
        return dateformatter.date(from: curr)!
    }
    
    var currentWeek: String{
        
        return "\(Calendar.current.component(.weekOfYear, from: Date()))"
    }
    
    var currentMonth: String{
        
        return "\(Calendar.current.component(.month, from: Date()))"
    }
    
    var currentYear: String{
        
        return "\(Calendar.current.component(.year, from: Date()))"
    }
}

var dateUtility:DateUtility? = DateUtility()

//MARK:- Network Model Protocol Inheritance

final class NetworkUtility:NetworkModel{
    
    
    
    func postRequest(api: String, parameters: [String : String], completion: @escaping (Any?) -> Void) {
        let url = URL(string:"\(kBaseUrl)\(api)")
        let headers = ["Content-Type":"application/json"]
        Alamofire.request(url!, method: .post, parameters: parameters, encoding: JSONEncoding.prettyPrinted, headers: headers).responseJSON {
            (response) in
            if response.result.isFailure
            {
                completion(nil)
            }
            else
            {
                completion(response.result.value!)
            }
        }
    }
    func putRequest(api: String, parameters: [String : String], completion: @escaping (Any?) -> Void) {
        let url = URL(string:"\(kBaseUrl)\(api)")
        let headers = ["Content-Type":"application/json"]
        Alamofire.request(url!, method: .put, parameters: parameters, encoding: JSONEncoding.prettyPrinted, headers: headers).responseJSON {
            (response) in
            if response.result.isFailure
            {
                completion(nil)
            }
            else
            {
                completion(response.result.value!)
            }
        }
    }
    
    func postWithRequest(api: String, parameters: [String : Any], completion: @escaping (Any?) -> Void) {
        
        let url = URL(string:"\(kBaseUrl)\(api)")
        let headers = ["Content-Type":"application/json"]
        
        Alamofire.request(url!, method: .post, parameters: parameters, encoding: JSONEncoding.prettyPrinted,headers: headers).responseJSON { (response) in
            
            if response.result.isFailure
            {
                completion(nil)
            }
            else
            {
                completion(response.result.value!)
            }
        }
    }
    
    func getRequest(api: String, parameters: [String : String], completion: @escaping (Any?) -> Void) {
        
        let url = URL(string:"\(kBaseUrl)\(api)")

        Alamofire.request(url!, method: .get, parameters: parameters, encoding: URLEncoding.default).responseJSON { (response) in
            
            if response.result.isFailure
            {
                completion(nil)
            }
            else
            {
                completion(response.result.value!)
            }
        }
    }
    func uploadfile(api: String, imageData: Data?, parameters: [String : Any], completion: @escaping ((Any?) -> Void), onError: @escaping (Error?) -> Void) {
        
        
        let url = URL(string:"\(kBaseUrl)\(api)")
        let headers: HTTPHeaders = [
            /* "Authorization": "your_access_token",  in case you need authorization header */
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            if let data = imageData{
                multipartFormData.append(data as Data, withName: "uploadfile", fileName: "abstractTemplate.pdf", mimeType: "application/pdf")
            }
            print(multipartFormData.contentLength)
            
        }, usingThreshold: UInt64.init(), to: url!, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    if let err = response.error{
                        onError(err)
                    }
                    else
                    {
                        completion(response.result.value)
                    }
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                onError(error)
            }
        }
    }
}

var networkUtility:NetworkUtility? = NetworkUtility()
    



