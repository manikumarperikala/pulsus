//
//  ConferencesApiManager.swift
//  ConferenceSeries
//
//  Created by Manikumar on 24/10/17.
//  Copyright © 2017 Omics. All rights reserved.
//

import UIKit
import Alamofire

class ConferenceApiManager: NSObject {

    static let sharedManager = ConferenceApiManager()
    
    func postRequest(api:String,parameters:[String:String],completion:@escaping(_ data:Any?)->Void)
    {
        let url = URL(string:"\(kBaseUrl)\(api)")
        Alamofire.request(url!, method: .post, parameters: parameters, encoding: URLEncoding.httpBody).responseJSON { (response) in
            
            if response.result.isFailure
            {
                completion(nil)
            }
            else
            {
                completion(response.result.value!)
            }
        }
    }
    
 func postWithRequest(api:String,parameters:[String:Any],completion:@escaping(_ data:Any?)->Void)
 {
    let url = URL(string:"\(kBaseUrl)\(api)")
    Alamofire.request(url!, method: .post, parameters: parameters, encoding: JSONEncoding.prettyPrinted).responseJSON { (response) in
        
        if response.result.isFailure
        {
            completion(nil)
        }
        else
        {
            completion(response.result.value!)
        }
    }
 }
    
    func getRequest(url:URL,parameters:[String:String],completion:@escaping(_ data:Any?)->Void)
    {
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default).responseJSON { (response) in
            
            if response.result.isFailure
            {
                completion(nil)
            }
            else
            {
                completion(response.result.value!)
            }
        }
    }
    
}
