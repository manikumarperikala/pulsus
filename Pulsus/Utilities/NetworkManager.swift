//
//  NetworkManager.swift
//  ConferenceSeries
//
//  Created by Manikumar on 21/02/18.
//  Copyright © 2018 Omics. All rights reserved.
//

import UIKit

final class NetworkManager: NSObject
{
    private let networkUtility:NetworkUtility
    
    // Injecting object
    init(utility:NetworkUtility) {
        self.networkUtility = utility
    }
    
    // Post request with 'String' type params
    func postApiWith(api:String,params:[String:String],completion:@escaping(_ data:Any?)->Void)
    {
         networkUtility.postRequest(api: api, parameters: params, completion: completion)
    }
    
    // Put request with 'String' type params
    func putApiWith(api:String,params:[String:String],completion:@escaping(_ data:Any?)->Void)
    {
        networkUtility.putRequest(api: api, parameters: params, completion: completion)
    }
    
    // Post request with 'Any' type params
    func postRequestApiWith(api:String,params:[String:Any],completion:@escaping(_ data:Any?)->Void){
        
        networkUtility.postWithRequest(api: api, parameters: params, completion: completion)
    }
    
    // Get request with 'String' type params
    func getRequestApiWith(api:String,params:[String:String],completion:@escaping(_ data:Any?)->Void)
    {
        networkUtility.getRequest(api: api, parameters: params, completion: completion)
    }
    
    // Document Upload with parametrs
       func uploadImageRequest(api:String,params:[String:Any],image:Data?,completion:@escaping (Any)-> Void,onerror:@escaping ((Error?) -> Void))
         {
             networkUtility.uploadfile(api: api, imageData: image!, parameters: params,completion: completion, onError: onerror)
         }
}
