//
//  FiltersViewController.swift
//  ConferenceSeries
//
//  Created by Srinu Babu Gedela on 23/05/20.
//  Copyright © 2020 Omics. All rights reserved.
//

import UIKit

protocol applyfilters {
    func filterData(conferencearr:[String:Any],isfilter:String)
}

class FiltersViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    
    
    @IBOutlet weak var tableview:UITableView!
    @IBOutlet weak var subjectBtn:UIButton!
    @IBOutlet weak var countryBtn:UIButton!
    @IBOutlet weak var monthBtn:UIButton!
    
    
    var subjectsData = [Subjects]()
    var monthsData = [Months]()
    var countriesData = [Countries]()
    var cities = [Cities]()
    var datatoload = [Any]()
    var selectedSubjects = [Subjects]()
    var activetitle = "Subject"
    var arrowLabel = UILabel()
    var filterdelegate:applyfilters?
    var isdropdownclicked = false
    private var openSection: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.getFilters()
        self.title = "Filters"
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.isTranslucent = false
    }

    func getFilters()
    {
        self.view.addActivity()
        let obj = NetworkManager(utility: networkUtility!)
               
        let params = ["page":1]
           
        obj.postRequestApiWith(api: kgetfilters, params: params){ (response) in
           if response != nil
           {
               let dict = response as! NSDictionary
               if dict.count > 0
               {
                   let status = iskeyexist(dict: dict, key: kstatus)
                   if status == "true" || status == "1"
                   {
                        if dict.value(forKey:"subjects") != nil
                        {
                            let subjectarr = dict.value(forKey: "subjects") as! NSArray
                            if subjectarr.count > 0
                            {
                                for (index,element) in subjectarr.enumerated()
                                {
                                    let innerval = ((element as! NSDictionary).value(forKey: "subject") as! String)
                                    let subject = Subjects(subject_id: "\(index)", subject_name: innerval,isSelected: false)
                                    self.subjectsData.append(subject)
                                }
                            }
                        }
                     if dict.value(forKey:"months") != nil
                      {
                          let subjectarr = dict.value(forKey: "months") as! NSArray
                          if subjectarr.count > 0
                          {
                              for (index,element) in subjectarr.enumerated()
                              {
                                  let innerval = ((element as! NSDictionary).value(forKey: "month") as! String)
                                let month =  Months(month_id: "\(index)", month_name: innerval, isSelected: false)
                                  self.monthsData.append(month)
                              }
                          }
                      }
                    if dict.value(forKey:"country_cities") != nil
                    {
                        let subjectarr = dict.value(forKey: "country_cities") as! NSArray
                         var citydict = [Cities]()
                        if subjectarr.count > 0
                        {
                            for dict in subjectarr
                            {
                               
                               let innerdict = dict as! NSDictionary
                               let country_id = innerdict.value(forKey: "country_id") as! String
                               let country_name = innerdict.value(forKey: "country_name") as! String
                                let cityarr = innerdict.value(forKey: "cities") as! NSArray
                                citydict.removeAll()
                                
                                for dictin in cityarr
                                {
                                    let city_id = (dictin as! NSDictionary).value(forKey: "city_id") as! String
                                    let city_name = ((dictin as! NSDictionary).value(forKey: "city_name") as! String)
                                    citydict.append(Cities(city_id: city_id, city_name: city_name, isSelected: false))
                                    self.cities.append(Cities(city_id: city_id, city_name: city_name, isSelected: false))
                                }
                                self.countriesData.append(Countries(country_id: country_id, country_name: country_name, cities: citydict, isSelected: false,collapsed: false))
                                
                            }
                         }
                       }
                    }
                 }
              
               DispatchQueue.main.async {
                   self.view.removeActivity()
                self.loadData(title:"Subject")
               }
       
           }
           else
           {
               DispatchQueue.main.async {
                   self.view.removeActivity()
               
               }
           }
    
}
}
    func loadData(title:String)
    {
        switch title {
        case "Subject":
            self.datatoload = subjectsData
            self.activetitle = "Subject"
            break
        case "Months":
            self.datatoload = monthsData
            self.activetitle = "Months"
            break
        case "Country":
            self.datatoload = countriesData
            self.activetitle = "Country"
            break
        default:
             self.datatoload = subjectsData
             self.activetitle = "Subject"
            break
        }
        DispatchQueue.main.async {
            self.tableview.reloadData()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if activetitle == "Country"
        {
            return datatoload.count
        }
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if  activetitle == "Country"
        {
                if ((datatoload as! [Countries])[section].collapsed == true)
                 {
                    return (((datatoload as! [Countries])[section]).cities.count)
                  
                 }
               else {
                   return 0
               }
                       
          /*  if ((datatoload as! [Countries])[section].collapsed == true)
           {
              return (((datatoload as! [Countries])[section]).cities.count)
            
           }
           else
           {
               let citiesselected = ((datatoload as! [Countries])[section]).cities
               for city in citiesselected
               {
                    if city.isSelected
                    {
                       return (((datatoload as! [Countries])[section]).cities.count)
                    }
               }
            return 0
            }
           */
        }
        if datatoload.count > 0
        {
            return datatoload.count
            
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! filterCell1
        
        switch activetitle {
        case "Subject":
            let data = datatoload as! [Subjects]
            let subject = data[indexPath.row]
            cell.title.text = subject.subject_name
            cell.img.layer.borderColor = UIColor.black.cgColor
            cell.img.layer.borderWidth = 1.0
            if subject.isSelected
            {
                cell.img.image = UIImage(imageLiteralResourceName: "tick")
            }
            else
            {
                 cell.img.image = nil
            }
            
        case "Months":
            let data = datatoload as! [Months]
            let subject = data[indexPath.row]
            cell.title.text = subject.month_name
            cell.img.layer.borderColor = UIColor.black.cgColor
            cell.img.layer.borderWidth = 1.0
            if subject.isSelected
               {
                   cell.img.image = UIImage(imageLiteralResourceName: "tick")
               }
               else
               {
                    cell.img.image = nil
               }
        case "Country":
            let data = datatoload as! [Countries]
            let subject = data[indexPath.section]
            let cities = subject.cities[indexPath.row]
            cell.title.text = cities.city_name
            cell.img.layer.borderColor = UIColor.black.cgColor
            cell.img.layer.borderWidth = 1.0
            if cities.isSelected
               {
                   cell.img.image = UIImage(imageLiteralResourceName: "tick")
               }
               else
               {
                    cell.img.image = nil
               }
            
        default:
            break
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          if activetitle == "Subject"
          {
             let data = datatoload as! [Subjects]
            ((data[indexPath.row])).isSelected = true
            let cell = self.tableview.cellForRow(at: indexPath) as! filterCell1
            cell.img.image = UIImage(imageLiteralResourceName: "tick")
            
          }
        if activetitle == "Months"
        {
           let data = datatoload as! [Months]
          ((data[indexPath.row])).isSelected = true
            let cell = self.tableview.cellForRow(at: indexPath) as! filterCell1
            cell.img.image = UIImage(imageLiteralResourceName: "tick")
        }
        if activetitle == "Country"
        {
           let data = datatoload as! [Countries]
           let subject = data[indexPath.section]
           let cities = subject.cities[indexPath.row]
           (cities).isSelected = true
            let cell = self.tableview.cellForRow(at: indexPath) as! filterCell1
            cell.img.image = UIImage(imageLiteralResourceName: "tick")
        }
        
        
         
        
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath)
    {
        if activetitle == "Subject"
        {
           let data = datatoload as! [Subjects]
          ((data[indexPath.row])).isSelected = false
            let cell = self.tableview.cellForRow(at: indexPath) as! filterCell1
            cell.img.image = nil
          
        }
        if activetitle == "Months"
        {
           let data = datatoload as! [Months]
          ((data[indexPath.row])).isSelected = false
            let cell = self.tableview.cellForRow(at: indexPath) as! filterCell1
            cell.img.image = nil
          
        }
        if activetitle == "Country"
        {
            let data = datatoload as! [Countries]
            let subject = data[indexPath.section]
            let cities = subject.cities[indexPath.row]
            (cities).isSelected = false
             let cell = self.tableview.cellForRow(at: indexPath) as! filterCell1
             cell.img.image = nil
        }
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if activetitle == "Country"
        {
            return 40
        }
        return 0
    }
    
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
     /*   let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header") as? CollapsibleTableViewHeader ?? CollapsibleTableViewHeader(reuseIdentifier: "header")
               
               header.titleLabel.text = (datatoload as! [Countries])[section].country_name
               header.arrowLabel.text = ">"
            //   header.setCollapsed(sections[section].collapsed)
               
               header.section = section
               header.delegate = self
               
               return header*/
        
                 let headerView = UIView()
                 headerView.backgroundColor = UIColor.white
                 headerView.tag  = section
                 let tap = UITapGestureRecognizer(target: self, action:#selector(self.sectionTap(_:)))
                 headerView.addGestureRecognizer(tap)
                 let arrowbtn = UIButton(frame: CGRect(x: 5, y: 10, width: 12, height: 12))
                 if (datatoload as! [Countries])[section].collapsed
                 {
                     arrowbtn.isSelected = true
                 }
                 arrowbtn.setImage(UIImage(named : "right"), for: UIControl.State.normal)
                 arrowbtn.setImage(UIImage(named : "down"), for: UIControl.State.selected)
                 headerView.addSubview(arrowbtn)
                 let headerLabel = UILabel(frame: CGRect(x: 25, y: 10, width:
                     tableView.bounds.size.width-50, height: tableView.bounds.size.height))
                 headerLabel.font = UIFont(name: kAppFontBold, size: 15)
                 headerLabel.textColor = UIColor.darkGray
                 headerLabel.text = (datatoload as! [Countries])[section].country_name
                 headerLabel.translatesAutoresizingMaskIntoConstraints = false
                 headerLabel.sizeToFit()
                 headerView.addSubview(headerLabel)
        
       
        
        let btn = UIButton(frame: CGRect(x: tableView.bounds.size.width-40
            , y: 10, width: 15, height: 15))
        btn.layer.borderColor = UIColor.darkGray.cgColor
        if (datatoload as! [Countries])[section].isSelected
        {
            btn.isSelected = true
        }
        btn.setImage(nil, for: UIControl.State.normal)
        btn.setImage(UIImage(named : "tick"), for: UIControl.State.selected)
        
        btn.layer.borderWidth = 1.0
        btn.tag = section
        btn.addTarget(self, action: #selector(selectCountry), for:   .primaryActionTriggered)
        headerView.addSubview(btn)
    
                 return headerView
             }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.darkGray
        return view
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
   
    @objc func selectCountry(_ sender:UIButton)
    {
        if sender.isSelected == true
        {
         
            let data = datatoload as! [Countries]
            ((data[sender.tag])).isSelected = false
            ((data[sender.tag])).collapsed = false
            let city = ((data[sender.tag])).cities
               if city.count > 0
               {
                  let _ = city.map({
                       $0.isSelected = false
                   })
               }
            
            sender.isSelected = false
            
        }
        
        else
        {
            let data = datatoload as! [Countries]
            ((data[sender.tag])).isSelected = true
            ((data[sender.tag])).collapsed = true
            let city = ((data[sender.tag])).cities
            if city.count > 0
            {
               let _ = city.map({
                    $0.isSelected = true
                })
            }
            sender.isSelected = true
        }
        DispatchQueue.main.async {
            self.tableview.reloadSections(NSIndexSet(index: sender.tag) as IndexSet, with: .none)
        }
        
    }
    //when the user taps on the section
      @objc func sectionTap(_ sender: UITapGestureRecognizer) {
          
          let newSection = sender.view!.tag
          
          //only do anything if new section is different
             let data = datatoload as! [Countries]
             let collapsible = data[newSection].collapsed
             if collapsible == true
             {
                data[newSection].collapsed = false
             }
             else
             {
                data[newSection].collapsed = true
             }
             var sectionArray: [Int] = [openSection]
             sectionArray.append(newSection)
             openSection = newSection
            
             //list of original open section and the new section that we need to expand
             let indices: IndexSet = IndexSet(sectionArray)
             //refresh those sections
             tableview.reloadSections(indices, with: .automatic)
      }
  
   
    @IBAction func btnSelection(_ sender:UIButton)
    {
        switch sender.tag {
        case 50:
            subjectBtn.backgroundColor =  UIColor.white
            monthBtn.backgroundColor = UIColor(red:230/255 , green: 230/255, blue: 230/255, alpha: 1.0)
            countryBtn.backgroundColor = UIColor(red:230/255 , green: 230/255, blue: 230/255, alpha: 1.0)
            self.loadData(title: "Subject")
            
            break
        case 60:
            subjectBtn.backgroundColor = UIColor(red:230/255 , green: 230/255, blue: 230/255, alpha: 1.0)
            monthBtn.backgroundColor = UIColor.white
            countryBtn.backgroundColor = UIColor(red:230/255 , green: 230/255, blue: 230/255, alpha: 1.0)
            self.loadData(title: "Months")
            break
        case 70:
           subjectBtn.backgroundColor = UIColor(red:230/255 , green: 230/255, blue: 230/255, alpha: 1.0)
           monthBtn.backgroundColor = UIColor(red:230/255 , green: 230/255, blue: 230/255, alpha: 1.0)
           countryBtn.backgroundColor = UIColor.white
           self.loadData(title: "Country")
        default:
            subjectBtn.backgroundColor =  UIColor.white
            monthBtn.backgroundColor = UIColor(red:230/255 , green: 230/255, blue: 230/255, alpha: 1.0)
            countryBtn.backgroundColor = UIColor(red:230/255 , green: 230/255, blue: 230/255, alpha: 1.0)
            break
        }
    }
    func setCollapsed(_ collapsed: Bool) {
          //
          // Animate the arrow rotation (see Extensions.swf)
          //
          arrowLabel.rotate(collapsed ? 0.0 : .pi / 2)
      }
    

    @IBAction func apply(_ sender:UIButton)
    {
        var filterstr = [String:Any]()
        var selectedcities = [Cities]()
        var selectedSubjects:[Subjects]
        {
            return (subjectsData).filter({$0.isSelected == true})
        }
        var selectedMonths:[Months]
        {
            return (monthsData).filter({$0.isSelected == true})
        }
        if countriesData.count > 0
        {
            selectedcities.removeAll()
            for dict in countriesData
            {
                let citydict = dict.cities
                var selectedcity:[Cities]
                {
                       return (citydict).filter({
                           $0.isSelected == true
                       })
                }
                selectedcities.append(contentsOf: selectedcity)
            }
           
        }
        
        if selectedSubjects.count > 0
        {
            var subjectstr = String()
            for (index,element) in selectedSubjects.enumerated()
            {
                subjectstr.append(contentsOf: element.subject_name)
                if (index < (selectedSubjects.count - 1))
                {
                    subjectstr.append(contentsOf: "###")
                }
            }
            filterstr.updateValue(subjectstr, forKey: "subjects")
        }
        if selectedMonths.count > 0
           {
               var subjectstr = String()
               for (index,element) in selectedMonths.enumerated()
               {
                subjectstr.append(contentsOf: element.month_name)
                   if (index < (selectedMonths.count - 1))
                   {
                       subjectstr.append(contentsOf: "###")
                   }
               }
               filterstr.updateValue(subjectstr, forKey: "months")
           }
        if selectedcities.count > 0
        {
            var subjectstr = String()
            for (index,element) in selectedcities.enumerated()
            {
                subjectstr.append(contentsOf: element.city_name)
                if (index < (selectedcities.count - 1))
                {
                    subjectstr.append(contentsOf: "###")
                }
            }
            
            filterstr.updateValue(subjectstr, forKey: "cities")
        }
        
       // self.getconferencesbyfilters(filters:filterstr)
        filterstr.updateValue("true", forKey: "filters")
        filterstr.updateValue(1, forKey: "page")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Home") as! HomeViewController
        vc.filterData(conferencearr:filterstr , isfilter: "true")
        isfiltersapplied = "true"
        self.navigationController?.pushViewController(vc, animated: false)
//         self.navigationController?.popViewController(animated: true)
        
    }
    
    func getconferencesbyfilters(filters:[String:Any])
    {
        if filters.count > 0
        {
         
                    self.view.addActivity()
                    let obj = NetworkManager(utility: networkUtility!)
                           
                    let params = filters
                       
                    obj.postRequestApiWith(api: kgetconfernecesByfilters, params: params){ (response) in
                       if response != nil
                       {
                           let dict = response as! NSDictionary
                           if dict.count > 0
                           {
                               let status = iskeyexist(dict: dict, key: kstatus)
                               if status == "true" || status == "1"
                               {
                                let conferences = dict.value(forKey: "conferences") as! NSArray
                                let filterconf = NSMutableArray()
                                for dict in conferences
                                {
                                    filterconf.add(dict)
                                }
                               
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                            //    vc.filterData(conferencearr: filterconf, isfilter: "true")
                                isfiltersapplied = "true"
                                
                                 self.navigationController?.popViewController(animated: true)
                                
                               }
                           }
                          
                           DispatchQueue.main.async {
                               self.view.removeActivity()
                           
                            
                           }
                   
                       }
                       else
                       {
                           DispatchQueue.main.async {
                               self.view.removeActivity()
                           
                           }
                       }
                
            }
            
            
        }
        else
        {
            // Alert user
        }
    }
    @IBAction func back(_ sender: UIBarButtonItem) {
        isfiltersapplied = "false"
        self.navigationController?.popViewController(animated: true)
        
    }
}


extension UIView {

    func rotate(_ toValue: CGFloat, duration: CFTimeInterval = 0.2) {
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        
        animation.toValue = toValue
        animation.duration = duration
        animation.isRemovedOnCompletion = false
        animation.fillMode = CAMediaTimingFillMode.forwards
        
        self.layer.add(animation, forKey: nil)
    }

}/*
extension FiltersViewController: CollapsibleTableViewHeaderDelegate {
    
    func toggleSection(_ header: CollapsibleTableViewHeader, section: Int) {
         let collapsed = !(datatoload as! [Countries])[section].collapsed
        /*let currentsection = section
        var i = 0
        for item in sections
        {
            if currentsection != i
            {
                if item.collapsed == false
                {
                 let obj = Section(name: sections[i].name, items: sections[i].items, collapsed: true)
                 sections[i] = obj
                }
            }
            i = i+1
        }*/
        
        // Toggle collapse
        (datatoload as! [Countries])[section].collapsed = collapsed
       
       // header.setCollapsed(collapsed)
        header.contentView.backgroundColor = UIColor(red: 22/255, green: 77/255, blue: 90/255, alpha: 1.0)
       // tableView.reloadData()
        tableview.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
    }
    
}*/
