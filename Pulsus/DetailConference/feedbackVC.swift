//
//  feedbackVC.swift
//  ConferenceSeries
//
//  Created by Srinu Babu Gedela on 08/06/20.
//  Copyright © 2020 Omics. All rights reserved.
//

import UIKit

class feedbackVC: UIViewController,UITextViewDelegate {

    
    @IBOutlet weak var textview: UITextView!
    
    var isfromsignin = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Feedback"
       
        textview.layer.cornerRadius = 2.0
        textview.layer.borderWidth = 1.0
        textview.layer.borderColor = UIColor.darkGray.cgColor
        textview.delegate = self
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
   
        self.textview.text = "Enter Your Feedback!"
        self.textview.textColor = UIColor.lightGray
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
    }
    
    @IBAction func sendfeedback(_ sender: UIButton) {
        let id = userDefaults.value(forKey: "conf_id") as! String
        print(id)
        
    }
    
    // Textview Delegate Method
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray
        {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty
        {
            textView.text = "Enter Your Feedback!"
            textView.textColor = UIColor.lightGray
        }
    }
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
}
