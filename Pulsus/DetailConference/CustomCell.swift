//
//  CustomCell.swift
//  ConferenceSeries
//
//  Created by Srinu Babu Gedela on 10/06/20.
//  Copyright © 2020 Omics. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {

    @IBOutlet weak var textview: UITextView!
    @IBOutlet weak var txtVwWidthConst: NSLayoutConstraint!
    @IBOutlet weak var titleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
