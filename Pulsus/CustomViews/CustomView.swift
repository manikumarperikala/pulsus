//
//  CustomView.swift
//  Conference Series
//
//  Created by Manikumar on 29/11/17.
//  Copyright © 2017 Omics. All rights reserved.
//

import UIKit

@IBDesignable
 class CustomView: UIView {

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBInspectable
    public var borderWidth:CGFloat = 0.5
    {
        didSet{
            self.layer.borderWidth = self.borderWidth
            self.layer.borderColor = UIColor.black.cgColor
        }
    }
    @IBInspectable
    public var borderColor:UIColor = UIColor.black
    {
        didSet{
            self.layer.borderColor = borderColor.cgColor
        }
    }
    @IBInspectable
    public var cornerRadius:CGFloat = 1.0
    {
        didSet{
            self.layer.cornerRadius = self.cornerRadius
        }
    }
    @IBInspectable
    public var shadowRadius:CGFloat = 1.0
    {
        didSet
        {
            self.layer.shadowRadius = self.shadowRadius
        }
    }
    @IBInspectable
    public var shadowColor:UIColor = UIColor.black
    {
        didSet
        {
            self.layer.shadowColor = self.shadowColor.cgColor
        }
    }

}
class ButtonBorder: UIButton
{
       var imageIconView = UIImageView()
       var paddingview = UIView()
       let padding = 0
       let size = 10
    override func awakeFromNib() {
        super.awakeFromNib()
               paddingview = UIView(frame: CGRect(x: -15, y: 0, width: size+padding, height: size))
               imageIconView = UIImageView(frame: CGRect(x: 0, y: 0, width: size, height: size))
               paddingview.addSubview(imageIconView)
                
//               self.rightViewMode = .always
//               self.rightView = paddingview
      }
      
      @IBInspectable
      public var borderWidth:CGFloat = 0.5
      {
          didSet{
              self.layer.borderWidth = self.borderWidth
              self.layer.borderColor = UIColor.black.cgColor
          }
      }
      @IBInspectable
      public var borderColor:UIColor = UIColor.black
      {
          didSet{
              self.layer.borderColor = borderColor.cgColor
          }
      }
      @IBInspectable
      public var cornerRadius:CGFloat = 1.0
      {
          didSet{
              self.layer.cornerRadius = self.cornerRadius
          }
      }
      @IBInspectable
      public var shadowRadius:CGFloat = 1.0
      {
          didSet
          {
              self.layer.shadowRadius = self.shadowRadius
          }
      }
      @IBInspectable
      public var shadowColor:UIColor = UIColor.black
      {
          didSet
          {
              self.layer.shadowColor = self.shadowColor.cgColor
          }
      }
   
    func setPaddingWithIcon(image:UIImage)
    {
        imageIconView.image = image
    }
    
    deinit {
        
    }

}

class LeftBarButton:UIBarButtonItem
{
    override init()
    {
        super.init()
        
        let btnLeftMenu: UIButton = UIButton()
    //    btnLeftMenu.setImage(UIImage(named: karrowback), for: .normal)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        btnLeftMenu.isUserInteractionEnabled = true
        self.customView = btnLeftMenu
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class RightBarButton:UIBarButtonItem
{
    override init()
    {
        super.init()
        
        let btnRightFilter: UIButton = UIButton()
    //    btnRightFilter.setImage(UIImage(named: kfilterIcon), for: .normal)
        btnRightFilter.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        btnRightFilter.isUserInteractionEnabled = true
        self.customView = btnRightFilter
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
