//
//  CustomDict.swift
//  ConferenceSeries
//
//  Created by manikumar on 12/03/20.
//  Copyright © 2020 Omics. All rights reserved.
//

import Foundation
import UIKit


class ProfileInfo:NSObject  {
    var fullname = String()
    var email = String()
    var userID = String()
    var token = String()
}

class ChoosenProducts:NSObject
{
    var conf_id = String()
    var conf_name = String()
    var total_Price:Int = 0
    var currency = String()
    var packages = [NSMutableDictionary]()
    var addons = [NSMutableDictionary]()
}
