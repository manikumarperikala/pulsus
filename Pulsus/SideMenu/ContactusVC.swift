//
//  ContactusVC.swift
//  ConferenceSeries
//
//  Created by Srinu Babu Gedela on 08/06/20.
//  Copyright © 2020 Omics. All rights reserved.
//

import UIKit
import MessageUI

class ContactusVC: UIViewController,UITableViewDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate,UITextFieldDelegate,sendDetails {
    
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableview:UITableView!
    @IBOutlet weak var menubutton:UIBarButtonItem!
    @IBOutlet weak var phonenumber:UITextField!
    @IBOutlet weak var query:UITextField!
    @IBOutlet weak var name:UITextField!
    @IBOutlet weak var email:UITextField!
    @IBOutlet weak var scrollview:UIScrollView!
    @IBOutlet weak var cntryBtn: ButtonBorder!
    var countrycode = String()

    
    var contactlist = [["name":"USA & Americas","email":"america@conferenceseries.com","number":"1-888-843-8169"],["name":"Asia-Pacific","email":"asia@conferenceseries.com","number":"44-203-7690-972"],["name":" Middle East","email":"me@conferenceseries.com","number":"1-201-380-5561"],["name":"Europe","email":"europe@conferenceseries.com","number":"44-0-800-014-8923"]]
    
        override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Contact Us"
        if revealViewController() != nil{
            menubutton.target = revealViewController()
            menubutton.action = #selector(SWRevealViewController.revealToggle(_:))
            view.addGestureRecognizer((self.revealViewController()?.panGestureRecognizer())!)
        }
            phonenumber.setBottomBorder(color: UIColor.darkGray)
            query.setBottomBorder(color: UIColor.darkGray)
            name.setBottomBorder(color: UIColor.darkGray)
            email.setBottomBorder(color: UIColor.darkGray)
            
            // Keyboard
            NotificationCenter.default.addObserver(
                       self,
                       selector: #selector(keyboardWillShow),
                       name: UIResponder.keyboardWillShowNotification,
                       object: nil
                   )
                   NotificationCenter.default.addObserver(
                              self,
                              selector: #selector(keyboardWillHide),
                              name: UIResponder.keyboardWillHideNotification,
                              object: nil
                          )
            
            
            let singleTap = UITapGestureRecognizer(target: self, action: Selector(("handleTap:")))
             singleTap.cancelsTouchesInView = false
             singleTap.numberOfTapsRequired = 1
             scrollview.addGestureRecognizer(singleTap)
           self.cntryBtn.titleEdgeInsets = UIEdgeInsets.init(top: 0, left: -(self.cntryBtn.imageView?.frame.size.width)!+(15), bottom: 0, right: (self.cntryBtn.imageView?.frame.size.width)!)
             self.cntryBtn.imageEdgeInsets = UIEdgeInsets.init(top: 15, left: self.cntryBtn.frame.size.width - 10, bottom: 15, right: 2);
            
            
    }
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
       return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
           self.navigationController?.navigationBar.barStyle = .black
       }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
       
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactlist.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 115
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ContactCell
        let data = (contactlist[indexPath.row] as NSDictionary)
        cell.name.text = data.value(forKey: "name") as? String
        cell.email.text = data.value(forKey: "email") as? String
        cell.number.text = data.value(forKey: "number") as? String
        cell.mailbtn.tag = indexPath.row
        cell.mailbtn.addTarget(self, action: #selector(sendmail(sender:)), for: .touchUpInside)
        cell.callbtn.tag = indexPath.row
        cell.callbtn.addTarget(self, action: #selector(callcontact(sender:)), for: .touchUpInside)
        return cell
    }
    @objc func keyboardWillShow(notification: NSNotification) {
          if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
              let keyboardHeight = keyboardSize.height
              var contentInset:UIEdgeInsets = self.scrollview.contentInset
                contentInset.bottom = keyboardHeight + 10
                scrollview.contentInset = contentInset
           
          }
      }
      @objc func keyboardWillHide(notification: NSNotification) {
          let contentInset:UIEdgeInsets = UIEdgeInsets.zero
             scrollview.contentInset = contentInset
          }
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        
    // handling code

        name.resignFirstResponder()
        phonenumber.resignFirstResponder()
        email.resignFirstResponder()
        query.resignFirstResponder()
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (phonenumber.text?.count)! >= 14
        {
            let  char = string.cString(using: String.Encoding.utf8)!
            let isBackSpace = strcmp(char, "\\b")
            
            if (isBackSpace == -92) {
                return true
            }
            
            return false
        }
       
        if textField.tag == 20
        {
         let valid = isValidEmailAddress(emailAddressString: textField.text!)
          if valid == true
          {
            email.setBottomBorder(color: UIColor.darkGray)
          }
          else
          {
            email.setBottomBorder(color: UIColor(red: 252/255, green: 78/255, blue: 8/255, alpha: 1.0))
          }
        }
        return true
    }
    
    //MARK:- Contact Us Service Integration
    
    @IBAction func sendcontacts( _ sender:UIButton)
    {
           self.view.endEditing(true)
           if ((name.text?.isEmpty)! || (query.text?.isEmpty)! || (phonenumber.text?.isEmpty)! || (email.text?.isEmpty)! || ((cntryBtn.currentTitle == "Select Country")))
           {
               self.showAlertWith(title: "Alert", message: "All fields are Mandatory")
           }
           else
           {
           if name.text == ""
           {
               self.showAlertWith(title:"Alert" , message: "Please enter the name")
           }
           
           if query.text == ""
           {
               self.showAlertWith(title:"Alert" , message: "Please enter your Research Interest")
           }
            if cntryBtn.currentTitle == "Select Country"
            {
                self.showAlertWith(title:"Alert" , message: "Please Select Country")
            }
          let validemail = (isValidEmailAddress(emailAddressString:(email.text!)))
          let validcontact = (isvalidcontact(value: ("\(self.countrycode)" + phonenumber.text!)))
          if validemail == false
          {
              // Show Alert message
              self.showAlertWith(title: "Alert", message: "Please enter valid EmailID")
          }
          if validcontact == false
          {
              // Show Alert message
              self.showAlertWith(title: "Alert", message: "Please enter valid Contact")
          }
               if ((validcontact) && (validemail))
               {

               let dateformatter = DateFormatter()
               dateformatter.dateFormat = kUTCformat
               let currentdate = dateformatter.string(from:Date())
               let params = ["name":self.name.text!,"email":email.text!,"contact":("\(self.countrycode)" + phonenumber.text!),"research":query.text!,"source":"ios","created_at":currentdate]
                self.sendcontactdetails(params: params)
            }
        }
    }
    
    
    func sendcontactdetails(params:[String:String])
    {
        self.view.addActivity()
        let obj = NetworkManager(utility: networkUtility!)
        obj.postRequestApiWith(api: kcontactus, params: params){ (response) in
          if response != nil
          {
            let dict = response as? NSDictionary ?? [:]
            if dict.count > 0
            {
                let status = iskeyexist(dict: dict, key: kstatus)
                if status == "1"
                {
                    self.showAlertWith(title: "Success", message: "Contact Details Successfully Submitted")
                }
                
                
            }
            DispatchQueue.main.async {
                self.view.removeActivity()
            }
              
          }
          else
          {
              DispatchQueue.main.async {
                  self.view.removeActivity()
              }
          }
                      
          }
    }
    
      @IBAction func CountryBtnTapped(_ sender: UIButton) {
           let storyBoard = UIStoryboard(name: "Main", bundle: nil)
           let viewTobeLoad = storyBoard.instantiateViewController(withIdentifier: "Country") as! SelectCountryVC
           viewTobeLoad.delegate2 = self
           viewTobeLoad.iscountry = true
           viewTobeLoad.isfromabstract = true
           viewTobeLoad.type = "country"
           self.navigationController?.pushViewController(viewTobeLoad, animated: true)
      }
    // Delegate Implementation
     func details(titlename: String,type:String,countrycode:String) {
         self.cntryBtn.setTitle(titlename, for: .normal)
         self.countrycode = countrycode
         self.navigationController?.popViewController(animated: false)
     }

    
    func isvalidcontact(value: String) -> Bool {
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", kContactFormat)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    func showAlertWith(title:String,message:String)
        {
            let alertController = UIAlertController(title: title, message:message , preferredStyle: .alert)
            //to change font of title and message.
            let titleFont = [NSAttributedString.Key.font: UIFont(name: kAppFontBold, size: 14.0)!]
            let messageFont = [NSAttributedString.Key.font: UIFont(name: kAppFont, size: 12.0)!]
            
            let titleAttrString = NSMutableAttributedString(string: title, attributes: titleFont)
            let messageAttrString = NSMutableAttributedString(string: message, attributes: messageFont)
            alertController.setValue(titleAttrString, forKey: "attributedTitle")
            alertController.setValue(messageAttrString, forKey: "attributedMessage")
            
            let alertAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
               
            })
    //        let titlealertaction = NSMutableAttributedString(string: "OK", attributes: titleFont)
    //        alertController.setValue(titlealertaction, forKey: "attributedTitle")
            alertController.addAction(alertAction)
            present(alertController, animated: true, completion: nil)
        }
    
    
    @objc func sendmail(sender: UIButton)
    {
        let mailid = ((contactlist[sender.tag] as NSDictionary).value(forKey: "email") as! String)
       if MFMailComposeViewController.canSendMail()
            {
                let mfvc = MFMailComposeViewController()
                mfvc.mailComposeDelegate = self
                mfvc.setToRecipients([mailid])
                present(mfvc, animated: true, completion: nil)
        }
        else
       {
         print("failed")
        }
        
    }
    @objc func callcontact(sender:UIButton)
    {
        let contactno = ((contactlist[sender.tag] as NSDictionary).value(forKey: "number") as! String)
        guard let number = URL(string: "tel://" + contactno) else { return }
            if #available(iOS 10.0, *) {
                if UIApplication.shared.canOpenURL(number)
                {
                  UIApplication.shared.open(number, options: [:])
                }
                else
                {
                    print("falied")
                }
                
            }else{
                if UIApplication.shared.canOpenURL(number)
               {
                 UIApplication.shared.openURL(number)
               }
               else
               {
                   print("falied")
               }
                
            }

    }

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
