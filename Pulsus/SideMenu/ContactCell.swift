//
//  ContactCell.swift
//  ConferenceSeries
//
//  Created by Srinu Babu Gedela on 08/06/20.
//  Copyright © 2020 Omics. All rights reserved.
//

import UIKit

class ContactCell: UITableViewCell {
    
    @IBOutlet weak var name:UILabel!
    @IBOutlet weak var email:UILabel!
    @IBOutlet weak var number:UILabel!
    @IBOutlet weak var mailbtn:UIButton!
    @IBOutlet weak var callbtn:UIButton!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }

}
