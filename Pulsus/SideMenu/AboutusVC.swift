//
//  AboutusVC.swift
//  ConferenceSeries
//
//  Created by Srinu Babu Gedela on 08/06/20.
//  Copyright © 2020 Omics. All rights reserved.
//

import UIKit

class AboutusVC: UIViewController {

    @IBOutlet weak var textview:UITextView!
    @IBOutlet weak var menubutton:UIBarButtonItem!
    
    override var preferredStatusBarStyle: UIStatusBarStyle
   {
      return .lightContent
   }
       
   override func viewDidAppear(_ animated: Bool) {
          self.navigationController?.navigationBar.barStyle = .black
      }
          
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "About Us"
        if revealViewController() != nil{
            menubutton.target = revealViewController()
            menubutton.action = #selector(SWRevealViewController.revealToggle(_:))
            view.addGestureRecognizer((self.revealViewController()?.panGestureRecognizer())!)
        }
        textview.textAlignment = .justified

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.textview.text = kAboutus
    }

}
