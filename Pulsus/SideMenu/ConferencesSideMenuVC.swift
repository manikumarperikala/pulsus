//
//  ConferencesSideMenuVC.swift
//  Conferences
//
//  Created by Manikumar on 25/10/17.
//  Copyright © 2017 TrakitNow. All rights reserved.
//

import UIKit


class ConferencesSideMenuVC: UITableViewController {
    var menulist = [String]()
     
    override func viewDidLoad() {
        super.viewDidLoad()
        //menulist = ["Conference.series.com","Home","SignOut"]
                self.tableView.backgroundColor = kAppNavColor
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.revealViewController().view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        

              if let loggedin = userDefaults.value(forKey: kLoggedIn)
                    {
                        if "\(loggedin)" == "true"
                        {
                            menulist = ["Conference.series.com","Home","About Us","Share","Contact Us","Sign Out"]
                        }
                        else
                        {
                            menulist = ["Conference.series.com","Home","About Us","Share","Contact Us"]
                        }
                    }
                    else
                    {
                        menulist = ["Conference.series.com","Home","About Us","Share","Contact Us"]
                    }
        menulist = ["Conference.series.com","Home","About Us","Share","Contact Us"]
        tableView.reloadData()
      
        
    }
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0
            {
                if UI_IDIOM == .pad{
                    return 100
                }
                return 70
            }
         else
        {
            if UI_IDIOM == .pad
            {
                return  60
            }
            return 50
        }
            
        }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menulist.count > 0 ? menulist.count : 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        var cell = SideMenuViewCell()
        
        if indexPath.row == 0
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath) as! SideMenuViewCell
            cell.backgroundColor = UIColor.white
            cell.titleLabel.textColor = .black
            cell.titleLabel.text = "Conference.series.com"
            cell.selectionStyle = .none
        }
        else
        {
            cell = tableView.dequeueReusableCell(withIdentifier: kCell, for: indexPath) as! SideMenuViewCell
            cell.titleLabel.textColor = .white
            cell.titleLabel.text = menulist[indexPath.row]
            if (menulist[indexPath.row]) == "Home"
            {
              cell.imagView.image = UIImage(imageLiteralResourceName: "home")
            }
            if (menulist[indexPath.row]) == "About Us"
            {
              cell.imagView.image = UIImage(imageLiteralResourceName: "about-us")
            }
            if (menulist[indexPath.row]) == "Share"
            {
              cell.imagView.image = UIImage(imageLiteralResourceName: "share")
            }
            if (menulist[indexPath.row]) == "Contact Us"
            {
              cell.imagView.image = UIImage(imageLiteralResourceName: "contact-us")
            }
            if (menulist[indexPath.row]) == "Sign Out"
           {
             cell.imagView.image = UIImage(imageLiteralResourceName: "logout")
           }
            cell.selectionStyle = .none
          
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let revealController = SWRevealViewController()
        tableView.deselectRow(at: indexPath as IndexPath, animated: false)
        
        switch "\(menulist[indexPath.row])" {
        case "Home":
            if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                sd.gotoDashboard()
            }
            else
            {
                print("from Home")
            }
        break
        case "About Us":
            let storyBoard = UIStoryboard(name: kMain, bundle: nil)
            let HomeVCController = storyBoard.instantiateViewController(withIdentifier: "AboutusNav") as? UINavigationController
            revealController.setFront(HomeVCController, animated: true)
            revealController.setFrontViewPosition(.leftSideMost, animated: true)
            self.revealViewController().pushFrontViewController(HomeVCController, animated: true)
            break
        case "Share":
            let shareText = "https://apps.apple.com/us/app/conference-series/id1510906225"
            let vc = UIActivityViewController(activityItems: [shareText], applicationActivities: nil)
            vc.popoverPresentationController?.sourceView = self.view
            vc.excludedActivityTypes =  [UIActivity.ActivityType.postToFacebook]
            present(vc, animated: true)
            break
        case "Contact Us":
            let storyBoard = UIStoryboard(name: kMain, bundle: nil)
            let HomeVCController = storyBoard.instantiateViewController(withIdentifier: "ContactNav") as? UINavigationController
            revealController.setFront(HomeVCController, animated: true)
            revealController.setFrontViewPosition(.leftSideMost, animated: true)
            self.revealViewController().pushFrontViewController(HomeVCController, animated: true)
            break
        case "Sign Out":
            
                self.showAlertWith(title: "Alert", message: "Do you want to SignOut")
         
            break
        default:
            print("conferenceseries")
            break
        }
    }
      func showAlertWith(title:String,message:String)
        {
            let alertController = UIAlertController(title: title, message:message , preferredStyle: .alert)
            //to change font of title and message.
            let titleFont = [NSAttributedString.Key.font: UIFont(name: kAppFontBold, size: 14.0)!]
            let messageFont = [NSAttributedString.Key.font: UIFont(name: kAppFont, size: 12.0)!]
            
            let titleAttrString = NSMutableAttributedString(string: title, attributes: titleFont)
            let messageAttrString = NSMutableAttributedString(string: message, attributes: messageFont)
            alertController.setValue(titleAttrString, forKey: "attributedTitle")
            alertController.setValue(messageAttrString, forKey: "attributedMessage")
            
            let alertAction = UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                userDefaults.set("false", forKey: kLoggedIn)
               
                if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                    sd.gotoDashboard()
                }
                
            })
            let alertAction1 = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                         
                       })
            
    //        let titlealertaction = NSMutableAttributedString(string: "OK", attributes: titleFont)
    //        alertController.setValue(titlealertaction, forKey: "attributedTitle")
            alertController.addAction(alertAction)
            alertController.addAction(alertAction1)
            present(alertController, animated: true, completion: nil)
        }
    
    
   /* func showAlert()
    {
        let alertController = UIAlertController(title: kLogout, message:kLogoutConfirmation , preferredStyle: .alert)
        let alertAction = UIAlertAction(title: kTypeYes, style: .default, handler: { (action) in
            
            appDelegate.gotoLoginPage()
            userDefaults.set(false, forKey: kLoggedInKey)
        })
        let cancelAction = UIAlertAction(title: kTypeNo, style: .destructive, handler: { (action) in
            
        })
        alertController.addAction(alertAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }*/
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "Profile"
//        {
//            let detailView = segue.destination as! ConferencesProfileVC
//            detailView.detailsObj = sender as! DeviceDetail
//        }
//    }
}

