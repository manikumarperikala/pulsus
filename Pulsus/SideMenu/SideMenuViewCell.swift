//
//  SideMenuViewCell.swift
//  Moskeet
//
//  Created by Manikumar on 30/10/17.
//  Copyright © 2017 TrakitNow. All rights reserved.
//

import UIKit

class SideMenuViewCell: UITableViewCell {
    
    @IBOutlet var imagView:UIImageView!
    @IBOutlet var titleLabel:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
