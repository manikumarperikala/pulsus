//
//  LoginViewController.swift
//  ConferenceSeries
//
//  Created by manikumar on 12/03/20.
//  Copyright © 2020 Omics. All rights reserved.
//

import UIKit
import GoogleSignIn
import AuthenticationServices



class LoginViewController: UIViewController,GIDSignInDelegate{
    
    @IBOutlet weak var appleSignInBtn: ASAuthorizationAppleIDButton!
    @IBOutlet weak var fbview: CustomView!
    @IBOutlet weak var registerBtn: UIButton!
    @IBOutlet weak var closeBtn: UIButton!
    var Profilelist = [ProfileInfo]()
    var confDict = NSDictionary()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        fbview.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
        self.registerBtn.layer.cornerRadius = 3.0
        self.registerBtn.layer.borderColor = (hexStringToUIColor(hex: "#071042")).cgColor
        self.registerBtn.layer.borderWidth = 1.0
        
        self.closeBtn.layer.cornerRadius = 3.0
        self.closeBtn.layer.borderColor = (hexStringToUIColor(hex: "#FF3B30")).cgColor
        self.closeBtn.layer.borderWidth = 1.0
        GIDSignIn.sharedInstance().presentingViewController = self
        self.navigationController?.navigationBar.isHidden = true
        // Automatically Signin the User
        //GIDSignIn.sharedInstance()?.restorePreviousSignIn()

        self.setupSOAppleSignIn()

        
    }
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

    @IBAction func gmailSignin(_ sender: UITapGestureRecognizer) {
       GIDSignIn.sharedInstance().delegate = self
       GIDSignIn.sharedInstance()?.signIn()
    }
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
          // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let email = user.profile.email
            let info = ProfileInfo()
            info.email = email!
            info.fullname = fullName!
            info.token = idToken!
            info.userID = userId!
            Profilelist.append(info)
            let storyBoard = UIStoryboard(name: kMain, bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "Registration") as! RegistrationViewController
            vc.isSignedfromGoogle = true
            vc.profileinfo = Profilelist
            vc.confDict = self.confDict
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else {
          print("\(error.localizedDescription)")
        }
        
    }
    
    
    @IBAction func facebookSignin(_ sender: UITapGestureRecognizer) {
        
    }
    @IBAction func close(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func Register(_ sender: UIButton)
    {
        self.gotoRegister()
    }
    func gotoRegister()
    {
        let storyBoard = UIStoryboard(name: kMain, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "Registration") as! RegistrationViewController
        vc.confDict = self.confDict
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func setupSOAppleSignIn() {

        let btnAuthorization = ASAuthorizationAppleIDButton()

        btnAuthorization.frame = CGRect(x: 0, y: 200, width: 200, height: 40)
        btnAuthorization.backgroundColor = UIColor.orange

        btnAuthorization.center = self.view.center

        appleSignInBtn.addTarget(self, action: #selector(actionHandleAppleSignin), for: .touchUpInside)

//        self.view.addSubview(btnAuthorization)

    }
    
    @objc func actionHandleAppleSignin() {

        let appleIDProvider = ASAuthorizationAppleIDProvider()

        let request = appleIDProvider.createRequest()

        request.requestedScopes = [.fullName, .email]

        let authorizationController = ASAuthorizationController(authorizationRequests: [request])

        authorizationController.delegate = self

        authorizationController.presentationContextProvider = self

        authorizationController.performRequests()

    }
    
}

extension LoginViewController: ASAuthorizationControllerDelegate {

 // ASAuthorizationControllerDelegate function for authorization failed

func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {

    print(error.localizedDescription)

}
    
       // ASAuthorizationControllerDelegate function for successful authorization

    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {

            if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
                print(authorization.credential)
                // Create an account as per your requirement
                print(appleIDCredential)
                let appleId = appleIDCredential.user

                let appleUserFirstName = appleIDCredential.fullName?.givenName
                
                let appleUserLastName = appleIDCredential.fullName?.familyName
                
                let appleUserEmail = appleIDCredential.email
                
                //Write your code
                let info = ProfileInfo()
                info.email = appleUserEmail ?? ""
                info.fullname = appleUserFirstName ?? "" + " " + (appleUserLastName ?? "")
                info.token = appleId
                info.userID = appleId
                Profilelist.append(info)
                let storyBoard = UIStoryboard(name: kMain, bundle: nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "Registration") as! RegistrationViewController
                vc.isSignedfromGoogle = true
                vc.profileinfo = Profilelist
                vc.confDict = self.confDict
                self.navigationController?.pushViewController(vc, animated: true)

            } else if let passwordCredential = authorization.credential as? ASPasswordCredential {

                let appleUsername = passwordCredential.user

                let applePassword = passwordCredential.password

                //Write your code

            }

        }

    }
    

extension LoginViewController: ASAuthorizationControllerPresentationContextProviding {

    //For present window

    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {

        return self.view.window!

    }

}


