//
//  SignUpViewController.swift
//  ConferenceSeries
//
//  Created by Srinu Babu Gedela on 15/05/20.
//  Copyright © 2020 Omics. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var name:UITextField!
    @IBOutlet weak var email:UITextField!
    @IBOutlet weak var password:UITextField!
    @IBOutlet weak var confpwd:UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        name.setBottomBorder(color: UIColor.darkGray)
        email.setBottomBorder(color: UIColor.darkGray)
        password.setBottomBorder(color: UIColor.darkGray)
        confpwd.setBottomBorder(color: UIColor.darkGray)
        NotificationCenter.default.addObserver(
                 self,
                 selector: #selector(keyboardWillShow),
                 name: UIResponder.keyboardWillShowNotification,
                 object: nil
             )
             NotificationCenter.default.addObserver(
                        self,
                        selector: #selector(keyboardWillHide),
                        name: UIResponder.keyboardWillHideNotification,
                        object: nil
                    )
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
           if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
               let keyboardHeight = keyboardSize.height
               if self.view.frame.origin.y == 0
               {
                   self.view.frame.origin.y = -50
               }
           }
       }
       @objc func keyboardWillHide(notification: NSNotification) {
           if self.view.frame.origin.y != 0
               {
                   self.view.frame.origin.y = 0
               }
          }
    @IBAction func tapped(_ sender:UITapGestureRecognizer)
    {
        self.view.endEditing(true)
    }
    
    @IBAction func submit(_ sender:UIButton)
    {
        // Signup service
    
        if ((name.text?.isEmpty)! || (email.text?.isEmpty)! || (password.text?.isEmpty)! || (confpwd.text?.isEmpty)!)
        {
            // Alert
            self.showAlertWith(title: "Alert", message: "All fields are mandatory")
        }
        else
        {
            let validemail = (isValidEmailAddress(emailAddressString:(email.text!)))
            if validemail == false
            {
                self.showAlertWith(title: "Alert", message: "Please enter valid emailID")
            }
            if ((password.text)! != (confpwd.text)!)
            {
                
                self.showAlertWith(title: "Alert", message: "Password & Confirm Password should be same")
            }
            if validemail && (password.text == confpwd.text)
            {
                self.view.addActivity()
                let obj = NetworkManager(utility: networkUtility!)
                      
                let params = ["name":self.name.text!,"email":email.text!,"password":self.password.text!,"source":"ios"]
                obj.postRequestApiWith(api: kappuser, params: params){ (response) in
                      if response != nil
                      {
                          let dict = response as! NSDictionary
                          if dict.count > 0
                          {
                            let status = iskeyexist(dict: dict, key: "status")
                            let message = iskeyexist(dict: dict, key: "message")
                            if status == "true" || status == "1"
                            {
                                
                                self.showAlertWith(title: "Success", message: "User Created successfully")
                            }
                            else
                            {
                                self.showAlertWith(title: "Failure", message: message)
                            }
                               
                          }
                          else
                          {
                              self.showAlertWith(title:"Failure", message: "User Creation failed")
                          }
                          DispatchQueue.main.async {
                              self.view.removeActivity()
                          }
                  
                      }
                      else
                      {
                          DispatchQueue.main.async {
                              self.view.removeActivity()
                          }
                      }
                
            }
    }
        }
    }
    
    @IBAction func signin(_ sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
     func showAlertWith(title:String,message:String)
        {
            let alertController = UIAlertController(title: title, message:message , preferredStyle: .alert)
            //to change font of title and message.
            let titleFont = [NSAttributedString.Key.font: UIFont(name: kAppFontBold, size: 14.0)!]
            let messageFont = [NSAttributedString.Key.font: UIFont(name: kAppFont, size: 12.0)!]
            
            let titleAttrString = NSMutableAttributedString(string: title, attributes: titleFont)
            let messageAttrString = NSMutableAttributedString(string: message, attributes: messageFont)
            alertController.setValue(titleAttrString, forKey: "attributedTitle")
            alertController.setValue(messageAttrString, forKey: "attributedMessage")
            
            let alertAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                if message == "Password & Confirm Password should be same"
                {
                    self.password.text = ""
                    self.confpwd.text = ""
                }
                if message == "User Created successfully"
                {
                    self.navigationController?.popViewController(animated: true)
                }
            })
    //        let titlealertaction = NSMutableAttributedString(string: "OK", attributes: titleFont)
    //        alertController.setValue(titlealertaction, forKey: "attributedTitle")
            alertController.addAction(alertAction)
            present(alertController, animated: true, completion: nil)
        }

}
extension SignUpViewController:UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
}
