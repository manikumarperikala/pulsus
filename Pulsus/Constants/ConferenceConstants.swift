//
//  ConferenceConstants.swift
//  ConferenceSeries
//
//  Created by manikumar on 06/03/20.
//  Copyright © 2020 Omics. All rights reserved.
//

import Foundation
import UIKit


let UI_IDIOM = UIDevice.current.userInterfaceIdiom
let appDelegate = UIApplication.shared.delegate as! AppDelegate
let scene = UIApplication.shared.connectedScenes.first
let userDefaults = UserDefaults.standard
let mainBundle = Bundle.main


//MARK:- Client IDS
let GoogleClientID = "590837996692-2ckcaq38k19qvsfvjpjrn84i05ln4eff.apps.googleusercontent.com"
// MARK:- Color codes & Fonts

let kAppNavColor = UIColor(red: 45/255, green: 52/255, blue: 54/255, alpha: 1.0)
let kAppFont = "ProximaNova-Regular"
let kAppFontBold = "ProximaNova-Bold"

//MARK:- BaseUrl

let kBaseUrl = "https://www.conferenceseries.com"

//MARK:- App Version
var kAppStoreVersion = ""
var kAppstoreLink = "itms-apps://itunes.apple.com/us/app/conference-series/id1510906225"


//MARK:- Api Service Urls
let kConferenceMobidata = "/api/get/mobiledata"
let kConferenceMobidataall = "/api/get/mobiledata_all"
let kConferenceWithID = "/api/get/mobiledata_conference"
let kProductDetails = "/api/conference/get_registration_products"
let kInsertPaymentRegistrattion = "/api/conference/insert_registration"
let kUpdatePaymentRegistration = "/api/conference/update_registration"
let kcreatePaymentIntent = "/stripe/public/create-payment-intent.php"
let kinsertabstract = "/api/conference/insert_abstract"
let ktracks = "/api/conference/get_tracks"
let kappuser = "/api/conference/insert_app_user"
let kappLogin = "/api/conference/app_user_login"
let kabstractTemplate = "/api/conference/get_abstract_template_file"
let kbrouchuredownld = "/api/conference/brochure_download"
let kgetfilters = "/api/conference/get_filters"
let kgetconfernecesByfilters = "/api/conference/get_conferences_by_filters"
let ksessiontracks = "/api/conference/get_track_details"
let kcontactus = "/api/get/mobiledata_insert_subscription"
let kAudioVideoSubmission = "/api/conference/audio_video_submission"


//MARK:- Date Formats
let kUTCformat = "yyyy-MM-dd"
let kRequiredDateFormat = "MMM dd, yyyy"
let kRequiredDateFormat1 = "MMMM dd,yyyy"


//MARK:- Static constants
let ktoken = "token"
let kActivityView = "ActivityView"
let kpage = "page"
let kconf_id = "conf_id"
let kregisteredPrd = "registration_products"
let kstatus = "status"
let kconferences = "conferences"
let ktitle = "title"
let kcity = "city"
let kcountry = "country"
let ksubject = "subject"
let kiconurl = "icon_url"
let kproductname = "productname"
let kimage_not_available = "image_not_available"
let kstartdate = "start_date"
let kenddate = "end_date"
let kdateExpired = "DateExpired"
let kdateavailble = "DateAvailble"
let kLoggedIn = "Logged_in"
let kappuserId = "app_user_id"
var isfiltersapplied = "false"
var isfromdetailconf = "false"


let kCell = "Cell"
let kMain = "Main"
let kDetails = "Details"
let kid = "id"
let kslider_url = "slider_url"
let kSearch = "SearchVC"


//MARK:- Hexcolors
let kdefalutcolor = "#E64040"
let kselectionColor = "#37b649"

// Formats
let kEmailFormat = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
//let kContactFormat = "^((\\+)|(00))[0-9]{12,12}$"
let kContactFormat = "^\\+(?:[0-9] ?){6,14}[0-9]$"
//
//Mark: Image
let imageUnchecked = UIImage.init(imageLiteralResourceName:"unchecked")
let imageChecked = UIImage.init(imageLiteralResourceName:"checked")


let stringAttributes = [
NSAttributedString.Key.font : UIFont(name: kAppFontBold, size: 12.0)!,
NSAttributedString.Key.foregroundColor : UIColor.darkGray,
NSAttributedString.Key.strikethroughColor : UIColor.darkGray
]
let titlearr = ["Select Title","Mr","Miss","Mrs","Prof","Dr","Assist Prof Dr","Assoc Prof Dr"]
let categoryarr = ["Select Category","Poster","Oral","Workshop"]

// Validate Emaild id
   func isValidEmailAddress(emailAddressString: String) -> Bool {
       
       var returnValue = true
       let emailRegEx = kEmailFormat
       
       do {
           let regex = try NSRegularExpression(pattern: emailRegEx)
           let nsString = emailAddressString as NSString
           let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
           
           if results.count == 0
           {
               returnValue = false
           }
           
       } catch let error as NSError {
           print("invalid regex: \(error.localizedDescription)")
           returnValue = false
       }
       
       return  returnValue
   }


//MARK:- About us text

let kAboutus = "Conference Series LLC LTD is an open resource platform that conducts 3000+ global events including International Conferences, Workshops, Symposia, Trade Shows, Exhibitions and Science Congresses in all the major scientific disciplines, including Clinical, Medical, pharmaceutical, Engineering, Technology, Business Management and Life Sciences across America, Europe, The Middle East, and Asia Pacific. It is reaching over 25 million researchers, scholars, students, professionals and corporate entities all over the globe.\n\nWorld renowned scientists, Noble laureates and scholars in their respective fields grace our events as keynote speakers, panel experts, and organizing committee members. Our speakers gain global visibility and recognition as we take them straight to the audience through live streaming that broadcasts your spoken words all over the globe instantaneously.\n\nWith its world class state of innovation and information knowhow, our conferences facilitate knowledge dissemination through round table discussions, debates, workshops and, poster presentations. Corporate entities, academic and research institutions benefit from the dignitaries of world class, including CEOs and policy makers and can ripe the benefits through B2B meetings, networking, product launching and promotion.\n\nWe encourage our authors and conference participants through research and travel grants, young research awards and provide assistance in career development and research guidance through 1000+ collaborations with NGOs, scientific and academic agencies, institutions that have signed MoU with us."
