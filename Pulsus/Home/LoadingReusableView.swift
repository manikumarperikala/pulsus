//
//  LoadingReusableView.swift
//  ConferenceSeries
//
//  Created by Srinu Babu Gedela on 29/05/20.
//  Copyright © 2020 Omics. All rights reserved.
//

import UIKit

class LoadingReusableView: UICollectionReusableView {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
    
}
