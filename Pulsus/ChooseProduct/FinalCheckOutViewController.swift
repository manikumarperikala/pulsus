//
//  FinalCheckOutViewController.swift
//  ConferenceSeries
//
//  Created by Mallesh Kurva on 18/04/20.
//  Copyright © 2020 Omics. All rights reserved.
//

import UIKit
import Stripe

class FinalCheckOutViewController: UIViewController,STPPaymentCardTextFieldDelegate, STPAuthenticationContext {
    
    func authenticationPresentingViewController() -> UIViewController {
        return self
    }
    

    let cardParams = STPPaymentMethodCardParams()
    var paymentIntentClientSecret: String?
    
    var registrationID = String()
    var payAmntStr = String()
    var conftitle = String()
    var paymentInsertionDict : NSDictionary = [:]
//  var productSelectedDict:NSMutableDictionary = [:]
    var productSelectDict = [NSMutableDictionary]()
    var selectedaAddOnProds = [NSMutableDictionary]()

    var currency = String()
    var stripeTokenStr = String()
    let scrollView = UIScrollView()
    var totalPayout = Int()
    
    var btnBuy = UIButton()
    
    let itemView = UIView()

//       @IBOutlet weak var btnBuy: UIButton!
//        @IBAction func OnPayBtnTapped(_ sender: UIButton) {
//            self.actionGetStripeToken()
//
//        }
        override func viewDidLoad() {
            super.viewDidLoad()
            
     //       currency = paymentInsertionDict.value(forKey: "currency") as! String
            currency = currency.uppercased()

            self.loadUIView()
            self.loadPaymentView()
            self.title = "CheckOut"
            

    //      paymentField.backgroundColor = UIColor.orange
            
            // Do any additional setup after loading the view.
        }
    
    func loadUIView() {
        
        scrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height - (60))
        
        self.view.addSubview(scrollView)

//        scrollView.backgroundColor = UIColor.orange
        
        //Item View
        
        itemView.frame = CGRect(x: 10, y: 10, width: scrollView.frame.size.width - (20), height: 100)
        scrollView.addSubview(itemView)
        
        // border radius
        // set the corner radius
        
        itemView.backgroundColor = UIColor.white
        itemView.layer.cornerRadius = 6.0
//        itemView.layer.masksToBounds = true
        itemView.layer.shadowColor = UIColor.lightGray.cgColor
        itemView.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        itemView.layer.shadowOpacity = 0.8
//        itemView.layer.shadowRadius = 4.0
        
        // set the cornerRadius of the containerView's layer
        itemView.layer.cornerRadius = 5
        itemView.layer.masksToBounds = false
        
//        saloonListDetailedView.backgroundColor = [CustomColors colorWithHexString:StandardWhiteColour];
//        saloonListDetailedView.layer.shadowOffset = CGSizeMake(0, 1.0);
//        saloonListDetailedView.layer.shadowOpacity = 0.8;
//        saloonListDetailedView.layer.masksToBounds = NO;
//        saloonListDetailedView.layer.shadowColor = [UIColor lightGrayColor].CGColor;



        let headerView = UIView()
        headerView.frame = CGRect(x: 0, y: 0, width: itemView.frame.size.width , height: 40)
        headerView.backgroundColor = UIColor.black
        itemView.addSubview(headerView)
        
        let itemLbl = UILabel()
        itemLbl.frame = CGRect(x: 10, y: 0, width: 100, height: 40)
        itemLbl.text = "Item"
        itemLbl.font = UIFont(name:kAppFont, size: 13)
        itemLbl.textColor = UIColor.white
        itemView.addSubview(itemLbl)
        
        let amountLbl = UILabel()
        amountLbl.frame = CGRect(x: itemView.frame.size.width - (100) , y: 0, width: 90, height: 40)
        amountLbl.text = "Amount"
        amountLbl.font = UIFont(name:kAppFont, size: 13)
        amountLbl.textColor = UIColor.white
        amountLbl.textAlignment = NSTextAlignment.right
        itemView.addSubview(amountLbl)
        
        var yValue = CGFloat()
        yValue = headerView.frame.origin.y+headerView.frame.size.height
        
        for i in 0...productSelectDict.count {
                  
            if( i < productSelectDict.count){
                
                let innerView = UIView()
                innerView.frame = CGRect(x: 0, y: yValue, width: itemView.frame.size.width, height: 55)
//                innerView.backgroundColor = UIColor.orange
                itemView.addSubview(innerView)
                
                let packageLbl = UILabel()
                packageLbl.frame = CGRect(x: 10, y: 5, width: innerView.frame.size.width - (80), height: 20)
                let pcgkey = "\("Package")\(i+1)"
                packageLbl.text = productSelectDict[i][pcgkey] as? String
                packageLbl.font = UIFont(name:kAppFont, size: 13)
                packageLbl.textColor = hexStringToUIColor(hex: "767676")
                innerView.addSubview(packageLbl)
                
                let categoryLbl = UILabel()
                categoryLbl.frame = CGRect(x: 10, y: packageLbl.frame.size.height+packageLbl.frame.origin.y , width: innerView.frame.size.width - (80), height: 20)
                let categoryKey = "\("Category")\(i+1)"
                categoryLbl.text = productSelectDict[i][categoryKey] as? String
                categoryLbl.font = UIFont(name:kAppFont, size: 13)
                categoryLbl.textColor = hexStringToUIColor(hex: "bebebe")
                innerView.addSubview(categoryLbl)
                
                let price = productSelectDict[i]["Price"] as! Int
                totalPayout = totalPayout + price
                
                let totalProdPrice = "\(productSelectDict[i]["Price"] ?? "0")"
//                totalProdPrice = productSelectDict[i]["Price1"] as! String
                
                let amountLbl = UILabel()
                amountLbl.frame = CGRect(x: innerView.frame.size.width - 80, y: innerView.frame.size.height/2-(10) , width: 70, height: 20)
//                categoryLbl.text = productSelectDict[i]["Category1"] as? String
                amountLbl.font = UIFont(name:kAppFont, size: 13)
                amountLbl.textColor = hexStringToUIColor(hex: "767676")
                
                amountLbl.textAlignment = NSTextAlignment.right
                innerView.addSubview(amountLbl)
                
//                amountLbl.backgroundColor = UIColor.orange

                if currency == "GBP"
                {
//                    amountLbl.text = "£\(String(describing: totalProdPrice))"
                    amountLbl.text = "£\(totalProdPrice)"
                    
                }
                if currency == "EURO"
                {
                    amountLbl.text = "€\(totalProdPrice)"
                }

                if currency == "USD"
                {
                    amountLbl.text = "$\(totalProdPrice)"
                }


                let seperatorLine = UILabel()
                seperatorLine.frame = CGRect(x: 0, y: innerView.frame.size.height - 1, width: innerView.frame.size.width , height: 1)
                seperatorLine.backgroundColor = UIColor.gray
                innerView .addSubview(seperatorLine)

                yValue = yValue + innerView.frame.size.height

            }
        }
        
        if(selectedaAddOnProds.count > 0){
        
            for i in 0...selectedaAddOnProds.count {
                
                if( i < selectedaAddOnProds.count){

                                                    let innerView = UIView()
                                                    innerView.frame = CGRect(x: 0, y: yValue, width: itemView.frame.size.width, height: 55)
                                    //                innerView.backgroundColor = UIColor.orange
                                                    itemView.addSubview(innerView)
                                                    
                                                    let packageLbl = UILabel()
                                                    packageLbl.frame = CGRect(x: 10, y: 5, width: innerView.frame.size.width - (80), height: 20)
                                                    let pcgkey = "\("AddOn")\(i)"
                                                    packageLbl.text = selectedaAddOnProds[i][pcgkey] as? String
                                                    packageLbl.font = UIFont(name:kAppFont, size: 13)
                                                    packageLbl.textColor = hexStringToUIColor(hex: "767676")
                                                    innerView.addSubview(packageLbl)
                                                    
                                                    let categoryLbl = UILabel()
                                                    categoryLbl.frame = CGRect(x: 10, y: packageLbl.frame.size.height+packageLbl.frame.origin.y , width: innerView.frame.size.width - (80), height: 20)
                    //                                let categoryKey = "\("Category")\(i+1)"
                                                    categoryLbl.text = selectedaAddOnProds[i]["title"] as? String
                                                    categoryLbl.font = UIFont(name:kAppFont, size: 13)
                                                    categoryLbl.textColor = hexStringToUIColor(hex: "bebebe")
                                                    innerView.addSubview(categoryLbl)
                                    
                                                   let priceKey = "\("Price")\(i)"
                     
                    //                (jsonDict["totalfup"] as! NSString).doubleValue
                                    
                                    let price = (selectedaAddOnProds[i][priceKey] as! NSString).intValue
                                                    totalPayout = totalPayout + Int(price)
                                                    
                                                    let totalProdPrice = "\(selectedaAddOnProds[i][priceKey] ?? "0")"
                                    //                totalProdPrice = productSelectDict[i]["Price1"] as! String
                                                    
                                                    let amountLbl = UILabel()
                                                    amountLbl.frame = CGRect(x: innerView.frame.size.width - 80, y: innerView.frame.size.height/2-(10) , width: 70, height: 20)
                                    //                categoryLbl.text = productSelectDict[i]["Category1"] as? String
                                                    amountLbl.font = UIFont(name:kAppFont, size: 13)
                                                    amountLbl.textColor = hexStringToUIColor(hex: "767676")
                                                    amountLbl.textAlignment = NSTextAlignment.right
                                                    innerView.addSubview(amountLbl)
                                                    
                                                    if currency == "GBP"
                                                    {
                                    //                    amountLbl.text = "£\(String(describing: totalProdPrice))"
                                                        amountLbl.text = "£\(totalProdPrice)"
                                                        
                                                    }
                                                    if currency == "EURO"
                                                    {
                                                        amountLbl.text = "€\(totalProdPrice)"
                                                    }

                                                    if currency == "USD"
                                                    {
                                                        amountLbl.text = "$\(totalProdPrice)"
                                                    }


                                                    let seperatorLine = UILabel()
                                                    seperatorLine.frame = CGRect(x: 0, y: innerView.frame.size.height - 1, width: innerView.frame.size.width , height: 1)
                                                    seperatorLine.backgroundColor = UIColor.gray
                                                    innerView .addSubview(seperatorLine)

                                                    yValue = yValue + innerView.frame.size.height

                }
            }
        
        }
        
        itemView.frame = CGRect(x: 10, y: 10, width: scrollView.frame.size.width - (20), height: yValue)
        
//      itemView.backgroundColor = UIColor.yellow
   
    }
        
    func loadPaymentView()  {
        
        //Payment View
        
        let paymentView = UIView()
        paymentView.frame = CGRect(x: 10, y: itemView.frame.origin.y+itemView.frame.size.height+(20) , width: scrollView.frame.size.width - (20), height: 170)
        scrollView.addSubview(paymentView)
        
                paymentView.backgroundColor = UIColor.white
                paymentView.layer.cornerRadius = 6.0
        //        itemView.layer.masksToBounds = true
                paymentView.layer.shadowColor = UIColor.lightGray.cgColor
                paymentView.layer.shadowOffset = CGSize(width: 0, height: 1.0)
                paymentView.layer.shadowOpacity = 0.8
        
//        paymentView.backgroundColor = UIColor.orange
        
        let paymentLbl = UILabel()
        paymentLbl.frame = CGRect(x: 10, y: 0, width: 150, height: 40)
        paymentLbl.text = "Paymnet Mode"
        paymentLbl.font = UIFont(name:kAppFont, size: 13)
        paymentLbl.textColor = hexStringToUIColor(hex: "767676")
        paymentView.addSubview(paymentLbl)
        
        
    let radioBtn = UIButton()
        radioBtn.frame = CGRect(x: 5, y: paymentLbl.frame.size.height+paymentLbl.frame.origin.y, width: 40, height: 40)
        radioBtn.setImage(UIImage.init(named:"radioActive"), for:.normal)
        radioBtn.imageEdgeInsets = UIEdgeInsets.init(top: 10, left: 10, bottom: 10, right: 10)
        paymentView.addSubview(radioBtn)
        
        let stripeLbl = UILabel()
        stripeLbl.frame = CGRect(x: radioBtn.frame.origin.x+radioBtn.frame.size.width, y: paymentLbl.frame.size.height+paymentLbl.frame.origin.y, width: 150, height: 40)
        stripeLbl.text = "Stripe"
        stripeLbl.font = UIFont(name:kAppFont, size: 13)
        stripeLbl.textColor = hexStringToUIColor(hex: "767676")
        paymentView.addSubview(stripeLbl)
        
        let paymentField = STPPaymentCardTextField(frame: CGRect(x: 10, y: stripeLbl.frame.origin.y+stripeLbl.frame.size.height, width:paymentView.frame.size.width - 20, height: 44))
        paymentField.delegate = self
        paymentView.addSubview(paymentField)
        
        let cardDetailsLbl = UILabel()
        cardDetailsLbl.frame = CGRect(x: 10, y: paymentField.frame.origin.y+paymentField.frame.size.height+(10), width: paymentView.frame.size.width - 20, height: 20)
        cardDetailsLbl.text = "Enter Card Details"
        cardDetailsLbl.font = UIFont(name:kAppFont, size: 11)
        cardDetailsLbl.textColor = hexStringToUIColor(hex: "767676")
        cardDetailsLbl.textAlignment = NSTextAlignment.right
        paymentView.addSubview(cardDetailsLbl)
        
        btnBuy = UIButton()
//        btnBuy.frame = CGRect(x: 10, y: self.view.frame.size.height - (170) , width: self.view.frame.size.width - (20), height: 50)
        btnBuy.frame = CGRect(x: 10, y:paymentView.frame.origin.y+paymentView.frame.size.height+(70)
         , width: self.view.frame.size.width - (20), height: 50)

        btnBuy.backgroundColor = hexStringToUIColor(hex: "36464e")
//        btnBuy.backgroundColor = UIColor.orange
        btnBuy.layer.cornerRadius = 5
        btnBuy.clipsToBounds = true
        btnBuy.setTitleColor(UIColor.white, for: .normal)
        btnBuy.isEnabled = false
        btnBuy.addTarget(self, action:#selector(payBtnTapped), for: .touchUpInside)
        self.view.addSubview(btnBuy)
        
        var payStr = String()
        
         payAmntStr = "\(totalPayout)"
        
                        if currency == "GBP"
                        {
        //                    amountLbl.text = "£\(String(describing: totalProdPrice))"
                            payStr = "Pay £\(payAmntStr)"
                            
                        }
                        if currency == "EURO"
                        {
                            payStr = "Pay €\(payAmntStr)"
                        }

                        if currency == "USD"
                        {
                            payStr = "Pay $\(payAmntStr)"
                        }
        
                       btnBuy.setTitle(payStr, for: .normal)

    }

    @objc func payBtnTapped() {
        if self.btnBuy.isEnabled
        {
             self.createPaymentIntent()
        }
    }
    func createPaymentIntent()
    {
        self.view.addActivity()
        let obj = NetworkManager(utility: networkUtility!)
        let email = userDefaults.value(forKey: "email")!
        
        let params = ["currency":currency,"amount":totalPayout,"description":self.conftitle,"receipt_email":email,"source":"ios"] as [String : Any]
       
         obj.postRequestApiWith(api: kcreatePaymentIntent, params: params){ (response) in
                    if response != nil
                    {
    
                        if response is NSDictionary
                        {
                            let dict = response as! NSDictionary
                            let publishableKey = dict.value(forKey: "publishableKey") as! String
                            
                             let clientSecret = dict.value(forKey: "clientSecret") as! String
                            Stripe.setDefaultPublishableKey(publishableKey)
                            self.paymentIntentClientSecret = clientSecret
                            self.actionGetStripeToken()
                        }

                    }
                    else
                    {
                        DispatchQueue.main.async {
                            self.view.removeActivity()
                        }
                    }

                }
    }
    
        func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
            print("Card number: \(String(describing: textField.cardParams.number)) Exp Month: \(String(describing: textField.cardParams.expMonth)) Exp Year: \(String(describing: textField.cardParams.expYear)) CVC: \(String(describing: textField.cardParams.cvc))")
        self.btnBuy.isEnabled = textField.isValid
        self.btnBuy.backgroundColor = hexStringToUIColor(hex: "36464e")

        if btnBuy.isEnabled {
        btnBuy.backgroundColor = UIColor.blue
        cardParams.number = textField.cardParams.number
            cardParams.expMonth = NSNumber(value: textField.cardParams.expMonth as! UInt)
            cardParams.expYear = NSNumber(value: textField.cardParams.expYear as! UInt)
        cardParams.cvc = textField.cardParams.cvc
        }
        }
        
        @IBAction func actionGetStripeToken() {
            
            guard let paymentIntentClientSecret = paymentIntentClientSecret else {
                    return;
                }
                // Collect card details
                let cardParams = self.cardParams
           
                let paymentMethodParams = STPPaymentMethodParams(card: cardParams, billingDetails: nil, metadata: nil)
                let paymentIntentParams = STPPaymentIntentParams(clientSecret: paymentIntentClientSecret)
                paymentIntentParams.paymentMethodParams = paymentMethodParams

                // Submit the payment
                let paymentHandler = STPPaymentHandler.shared()
                paymentHandler.confirmPayment(withParams: paymentIntentParams, authenticationContext: self) { (status, paymentIntent, error) in
                    
                    
                    switch (status) {
                    case .failed:
//                        self.displayAlert(title: "Payment failed", message: error?.localizedDescription ?? "")
                        
                        self.showAlertWith(title: "Payment failed", message: error?.localizedDescription ?? "")
//                        let paydetails = paymentIntent
//                        let stripeid = paydetails?.stripeId
//                        let status = paydetails?.status
                      //  self.updatePayentDetails(stripeId: "", status:"failed")
                        break
                    case .canceled:
                        self.showAlertWith(title: "Payment canceled", message: error?.localizedDescription ?? "")
//                        let paydetails = paymentIntent
//                        let stripeid = paydetails?.stripeId
//                        let status = paydetails?.status
                     //   self.updatePayentDetails(stripeId: "", status: "canceled")

//                        self.displayAlert(title: "Payment canceled", message: error?.localizedDescription ?? "")
                        break
                    case .succeeded:
                       // self.showAlertWith(title: "Payment succeeded", message: error?.localizedDescription ?? "")
                        let paydetails = paymentIntent
                        let stripeid = paydetails?.stripeId
                        let status = paydetails?.status
                        self.updatePayentDetails(stripeId: stripeid!, status: "succeeded")
//                        self.displayAlert(title: "Payment succeeded", message: paymentIntent?.description ?? "", restartDemo: true)
                        break
                    @unknown default:
                        fatalError()
                        break
                    }
                }
            }
            
           
          /*  STPAPIClient.shared().createToken(withCard: cardParams) { (token, error) in
                if let error = error {
                    // show the error to the user
                    print(error)
                    // self.showAlertButtonTapped(strTitle: "Error", strMessage: error.localizedDescription)
                    self.showAlertWith(title: "Error", message: error.localizedDescription)
                } else if let token = token {
                    
                    self.stripeTokenStr = "\(token)"
                    self.completecharge()
                    
                    // self.updatePayentDetails()
                    print(self.stripeTokenStr)
                    //Send token to backend for process
                }
            }*/
//        }
    
    func completecharge()
    {
       
    }
    

        //MARK:- AlerViewController
        func showAlertButtonTapped(strTitle:String, strMessage:String) {
        // create the alert
            let alert = UIAlertController(title: strTitle, message: strMessage, preferredStyle: UIAlertController.Style.alert)
        // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            
        // show the alert
            self.present(alert, animated: true, completion: nil)
        }


        /*
        // MARK: - Navigation

        // In a storyboard-based application, you will often want to do a little preparation before navigation
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            // Get the new view controller using segue.destination.
            // Pass the selected object to the new view controller.
        }
        */

        
        @IBAction func donate(sender: AnyObject) {

        }

    //MARK:- To get Conferences list
    func updatePayentDetails(stripeId:String,status:String)
        {
            self.view.addActivity()
            let obj = NetworkManager(utility: networkUtility!)
       
            let regID = paymentInsertionDict.value(forKey: "reg_id") as! String
            let currencyStr = paymentInsertionDict.value(forKey: "currency") as! String
            let statusStr = paymentInsertionDict.value(forKey: "paystatus")
           
            let appuserid = userDefaults.value(forKey: kappuserId)!
            let params = ["reg_id":regID,"currency":currencyStr,"id":stripeId,"status":status] as [String : Any]
            obj.postRequestApiWith(api: kUpdatePaymentRegistration, params: params){ (response) in
            if response != nil
            {
                if response is NSDictionary
                {
                    let dict = response as! NSDictionary
                    let status = iskeyexist(dict: dict, key: kstatus)
                    if status == "1"
                    {
//                                let storyBoard = UIStoryboard(name: kMain, bundle: nil)
//                                let vc = storyBoard.instantiateViewController(withIdentifier: "AbstractVC") as! AbstractViewController
//
//                        self.navigationController?.pushViewController(vc, animated: true)
                        
                        self.showAlertWith(title: "SUCCESS", message: "Your Payment is succcessfull")
                        
//                        if dict.value(forKey: "registration_details") != nil
//                        {
//
//                        }
                    }
                    else
                    {
                       self.showAlertWith(title: "Failure", message: "Your Payment Failed")
                    }

    //                if self.conferenceArray.count > 0
    //                {
    //                    DispatchQueue.main.async {
    //                        self.view.removeActivity()
    //                        self.collectionView.reloadData()
    //                    }
    //                }
    //                else
    //                {
    //                    print("Data Not availble")
    //                }
                }

            }
            else
            {
                DispatchQueue.main.async {
                    self.view.removeActivity()
                }
            }

        }
        }
     func showAlertWith(title:String,message:String)
        {
            let alertController = UIAlertController(title: title, message:message , preferredStyle: .alert)
            //to change font of title and message.
            let titleFont = [NSAttributedString.Key.font: UIFont(name: kAppFontBold, size: 14.0)!]
            let messageFont = [NSAttributedString.Key.font: UIFont(name: kAppFont, size: 12.0)!]
            
            let titleAttrString = NSMutableAttributedString(string: title, attributes: titleFont)
            let messageAttrString = NSMutableAttributedString(string: message, attributes: messageFont)
            alertController.setValue(titleAttrString, forKey: "attributedTitle")
            alertController.setValue(messageAttrString, forKey: "attributedMessage")
            
            let alertAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
              //  userDefaults.set("true", forKey: "PaymentDone")
                if title == "Payment failed" || title == "Payment canceled"
                {
                    self.view.removeActivity()
                }
                else
                {
                    if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                                                                     sd.checkstatus()
                                                                 }
                }
               
            })
    //        let titlealertaction = NSMutableAttributedString(string: "OK", attributes: titleFont)
    //        alertController.setValue(titlealertaction, forKey: "attributedTitle")
            alertController.addAction(alertAction)
            present(alertController, animated: true, completion: nil)
        }
    func showAlertWith1(title:String,message:String)
           {
               let alertController = UIAlertController(title: title, message:message , preferredStyle: .alert)
               //to change font of title and message.
               let titleFont = [NSAttributedString.Key.font: UIFont(name: kAppFontBold, size: 14.0)!]
               let messageFont = [NSAttributedString.Key.font: UIFont(name: kAppFont, size: 12.0)!]
               
               let titleAttrString = NSMutableAttributedString(string: title, attributes: titleFont)
               let messageAttrString = NSMutableAttributedString(string: message, attributes: messageFont)
               alertController.setValue(titleAttrString, forKey: "attributedTitle")
               alertController.setValue(messageAttrString, forKey: "attributedMessage")
               
               let alertAction = UIAlertAction(title: "YES", style: .default, handler: { (action) in
                 //  userDefaults.set("true", forKey: "PaymentDone")
                   if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                                                     sd.checkstatus()
                                                 }
               })
            let alertaction1 = UIAlertAction(title: "CANCEL", style: .cancel, handler: {
                (action) in
            })
       //        let titlealertaction = NSMutableAttributedString(string: "OK", attributes: titleFont)
       //        alertController.setValue(titlealertaction, forKey: "attributedTitle")
               alertController.addAction(alertAction)
               alertController.addAction(alertaction1)
               present(alertController, animated: true, completion: nil)
           }
    @IBAction func back(_ sender: UIBarButtonItem) {
        
        self.cancelpayment()
          
       }
    func cancelpayment()
    {
        self.showAlertWith1(title: "Alert", message: "Would you Like to cancel the Payment")
    }
}

