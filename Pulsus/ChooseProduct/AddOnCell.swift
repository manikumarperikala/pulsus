//
//  AddOnCell.swift
//  ConferenceSeries
//
//  Created by manikumar on 16/03/20.
//  Copyright © 2020 Omics. All rights reserved.
//

import UIKit

class AddOnCell: UICollectionViewCell {
    
    @IBOutlet weak var title:UILabel!
    @IBOutlet weak var price:UILabel!
    @IBOutlet weak var addon:UILabel!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    
}
