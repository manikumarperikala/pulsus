//
//  BroucherDownloadVC.swift
//  ConferenceSeries
//
//  Created by Srinu Babu Gedela on 21/05/20.
//  Copyright © 2020 Omics. All rights reserved.
//

import UIKit
import SVProgressHUD_0_8_1
import WebKit

class BroucherDownloadVC: UIViewController,sendDetails {

    @IBOutlet weak var titleBtn: ButtonBorder!
    @IBOutlet weak var cntryBtn: ButtonBorder!
    @IBOutlet weak var email:UITextField!
    @IBOutlet weak var phone:UITextField!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var company:UITextField!
    @IBOutlet weak var query:UITextField!
    var isfromsignin = false
    var activeTextField : UITextField? = nil
    var destinationpath:URL? = nil
    
    var countrycode = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        self.title = "Brouchure Download"
            self.titleBtn.titleEdgeInsets = UIEdgeInsets.init(top: 0, left: -(self.titleBtn.imageView?.frame.size.width)!+(15), bottom: 0, right: (self.titleBtn.imageView?.frame.size.width)!)
            self.titleBtn.imageEdgeInsets = UIEdgeInsets.init(top: 15, left: self.titleBtn.frame.size.width - 10, bottom: 15, right: 2);
                  
            self.cntryBtn.titleEdgeInsets = UIEdgeInsets.init(top: 0, left: -(self.cntryBtn.imageView?.frame.size.width)!+(15), bottom: 0, right: (self.cntryBtn.imageView?.frame.size.width)!)
            self.cntryBtn.imageEdgeInsets = UIEdgeInsets.init(top: 15, left: self.cntryBtn.frame.size.width - 10, bottom: 15, right: 2);
        NotificationCenter.default.addObserver(
                   self,
                   selector: #selector(keyboardWillShow),
                   name: UIResponder.keyboardWillShowNotification,
                   object: nil
               )
        NotificationCenter.default.addObserver(
                          self,
                          selector: #selector(keyboardWillHide),
                          name: UIResponder.keyboardWillHideNotification,
                          object: nil
                      )
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.name.text = ""
//        self.email.text = ""
//        self.titleBtn.setTitle("Select Title", for: .normal)
//        self.cntryBtn.setTitle("Select Country", for: .normal)
//        self.phone.text = ""
//        self.query.text = ""
//        self.company.text = ""
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
    }
    // Keyboard Notification Methods
    @objc func keyboardWillShow(notification: NSNotification) {
           if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
                 var shouldMoveViewUp = false
               
                 // if active text field is not nil
                 if let activeTextField = activeTextField {
               
                   let bottomOfTextField = activeTextField.convert(activeTextField.bounds, to: self.view).maxY;
                   
                   let topOfKeyboard = self.view.frame.height - keyboardSize.height
               
                   // if the bottom of Textfield is below the top of keyboard, move up
                   if bottomOfTextField > topOfKeyboard {
                     shouldMoveViewUp = true
                   }
                 }
               
                 if(shouldMoveViewUp) {
                   self.view.frame.origin.y = 0 - keyboardSize.height + 64
                    print(self.view.frame.origin.y)
                 }
//               let keyboardHeight = keyboardSize.height
//               if self.view.frame.origin.y == 0
//               {
//                self.view.frame.origin.y = self.view.frame.origin.y - 50
//               }
           }
       }
       @objc func keyboardWillHide(notification: NSNotification) {
        print(self.view.frame.origin.y)
        if (self.view.frame.origin.y <= 0)
               {
                if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                    self.view.frame.origin.y = self.view.frame.origin.y + keyboardSize.height
                }
                
               }
          }
    // Title Action Method
    @IBAction func TitleBtnTapped(_ sender: UIButton) {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let viewTobeLoad = storyBoard.instantiateViewController(withIdentifier: "Country") as! SelectCountryVC
            viewTobeLoad.delegate2 = self
            viewTobeLoad.iscountry = false
            viewTobeLoad.isfromabstract = true
            viewTobeLoad.fields = titlearr
            viewTobeLoad.type = "title"
            self.navigationController?.pushViewController(viewTobeLoad, animated: true)
      }
    // Country Action
    @IBAction func CountryBtnTapped(_ sender: UIButton) {
           let storyBoard = UIStoryboard(name: "Main", bundle: nil)
           let viewTobeLoad = storyBoard.instantiateViewController(withIdentifier: "Country") as! SelectCountryVC
           viewTobeLoad.delegate2 = self
           viewTobeLoad.iscountry = true
           viewTobeLoad.isfromabstract = true
           viewTobeLoad.type = "country"
           self.navigationController?.pushViewController(viewTobeLoad, animated: true)
      }
    
    // Delegate method
    func details(titlename: String, type: String, countrycode: String) {

           switch type{
           case "title":
               self.titleBtn.setTitle(titlename, for: .normal)
               break
           case "country":
               self.cntryBtn.setTitle(titlename, for: .normal)
               self.countrycode = countrycode
               break
           
           default:
               self.titleBtn.setTitle("Select Title", for: .normal)
               self.cntryBtn.setTitle("Select Country", for: .normal)
               break
           }
           self.navigationController?.popViewController(animated: false)
       }

    
    // Download Brouchure Action
      @IBAction func Downloadbrouchure(_ sender:UIButton)
      {
          self.view.endEditing(true)
        if (titleBtn.currentTitle == "Select Title") || (name.text?.isEmpty)! ||  cntryBtn.currentTitle == "Select Country" || (company.text?.isEmpty)! || (phone.text?.isEmpty)! || (email.text?.isEmpty)! || (query.text?.isEmpty)!
          {
              self.showAlertWith(title: "Alert", message: "All fields are Mandatory")
          }
          else
          {
          if titleBtn.currentTitle == "Select Title"
          {
              self.showAlertWith(title:"Alert" , message: "Please Select Title")
          }
          if name.text == ""
          {
              self.showAlertWith(title:"Alert" , message: "Please enter the name")
          }
          if cntryBtn.currentTitle == "Select Country"
          {
              self.showAlertWith(title:"Alert" , message: "Please Select Country")
          }
          if company.text == ""
          {
              self.showAlertWith(title:"Alert" , message: "Please enter Company/University")
          }
          if query.text == ""
          {
              self.showAlertWith(title:"Alert" , message: "Please enter Query")
          }
        
         let validemail = (isValidEmailAddress(emailAddressString:(email.text!)))
         let validcontact = (isvalidcontact(value: ("\(self.countrycode)" + phone.text!)))
         if validemail == false
         {
             // Show Alert message
             self.showAlertWith(title: "Alert", message: "Please enter valid EmailID")
         }
         if validcontact == false
         {
             // Show Alert message
             self.showAlertWith(title: "Alert", message: "Please enter valid Contact")
         }
              if ((validcontact) && (validemail))
            {

          let dateformatter = DateFormatter()
          dateformatter.dateFormat = kUTCformat
          let currentdate = dateformatter.string(from:Date())
          
          let id = userDefaults.value(forKey: "conf_id") as! String
          let fname = self.name.text!
          let email = self.email.text!
          let country = self.cntryBtn.currentTitle!
          let phone = self.phone.text!
         
          let params = ["conf_id":id,"title":self.titleBtn.currentTitle!,"name":"\(fname)","country":"\(country)","email":"\(email)","phone":"\(phone)","queries":self.query.text!,"company":self.company.text!,"date":currentdate,"source":"ios"] as [String : Any]
          self.downloadFile(params: params)
              }
          }
      }
    func downloadFile(params:[String:Any])
    {
        self.view.addActivity()
        let obj = NetworkManager(utility: networkUtility!)
        let params = params
        obj.postRequestApiWith(api: kbrouchuredownld, params: params){ (response) in
            if response != nil
            {
                
                let dict = response as! NSDictionary
                let status = iskeyexist(dict: dict, key: kstatus)
                if (status == "true" || status == "1")
                {
                    if  dict.value(forKey: "file") is NSArray
                    {
                    if((dict.value(forKey: "file") as! NSArray).count > 0)
                    {
                        let result = dict.value(forKey: "file") as! NSArray
                        let innerdict = result.firstObject as! NSDictionary
                        let templateurl = iskeyexist(dict: innerdict, key: "brochure_file")
                        if templateurl != ""
                        {
                            self.downloadsample(url:templateurl)
                        }
                        else
                        {
                            self.showAlertWith(title: "Alert", message: "Brouchure Template is not available")
                        }
                    }
                        else
                    {
                        self.showAlertWith(title: "Alert", message: "Brouchure Template is not available")
                        }
                    }
                    else
                    {
                       self.showAlertWith(title: "Alert", message: "Brouchure Template is not available")
                    }
                    
                }
                else
                {
                    self.showAlertWith(title: "Alert", message: "Brouchure Template is not available")
                }
                DispatchQueue.main.async {
                    self.view.removeActivity()
                }
        
            }
            else
            {
    
                DispatchQueue.main.async {
                    self.view.removeActivity()
                }
            }
        }
    }
    func downloadsample(url:String)
    {
        guard let url = URL(string: url) else { return }
               
        let urlSession = URLSession(configuration: .default, delegate: self, delegateQueue: OperationQueue())
               
        let downloadTask = urlSession.downloadTask(with: url)
        downloadTask.resume()
        loaddocument()
    }
    
    func loaddocument()
    {
        let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        if destinationpath != nil
        {
        let destinationURL = documentDirectory.appendingPathComponent(destinationpath!.lastPathComponent)
        let fileexists = FileManager().fileExists(atPath: destinationURL.path)
        if fileexists
        {
            DispatchQueue.main.async {
                
                let data = try! Data(contentsOf: destinationURL)
                let loadwebview = WKWebView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
                loadwebview.load(data, mimeType: "application/pdf", characterEncodingName: "", baseURL: destinationURL.deletingLastPathComponent())
                let docVC = UIViewController()
                docVC.view.addSubview(loadwebview)
                docVC.view.addActivity()
                
                loadwebview.translatesAutoresizingMaskIntoConstraints = false
                loadwebview.leftAnchor.constraint(equalTo: docVC.view.leftAnchor).isActive = true
                loadwebview.rightAnchor.constraint(equalTo: docVC.view.rightAnchor).isActive = true
                loadwebview.topAnchor.constraint(equalTo: docVC.view.topAnchor).isActive = true
                loadwebview.bottomAnchor.constraint(equalTo: docVC.view.bottomAnchor).isActive = true
                docVC.title = destinationURL.lastPathComponent
                self.navigationController?.pushViewController(docVC, animated: true)
                docVC.view.removeActivity()
            }
        }
        else
        {
           print("file not exists")
        }
        }
        else
        {
            self.showAlertWith(title: "Alert", message: "Brouchure Download failed")
        }
        
    }
    
    // Phone Validation
    func isvalidcontact(value: String) -> Bool {
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", kContactFormat)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    // Alert controller
    func showAlertWith(title:String,message:String)
        {
            let alertController = UIAlertController(title: title, message:message , preferredStyle: .alert)
            //to change font of title and message.
            let titleFont = [NSAttributedString.Key.font: UIFont(name: kAppFontBold, size: 14.0)!]
            let messageFont = [NSAttributedString.Key.font: UIFont(name: kAppFont, size: 12.0)!]
            
            let titleAttrString = NSMutableAttributedString(string: title, attributes: titleFont)
            let messageAttrString = NSMutableAttributedString(string: message, attributes: messageFont)
            alertController.setValue(titleAttrString, forKey: "attributedTitle")
            alertController.setValue(messageAttrString, forKey: "attributedMessage")
            
            let alertAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
               
            })
    //        let titlealertaction = NSMutableAttributedString(string: "OK", attributes: titleFont)
    //        alertController.setValue(titlealertaction, forKey: "attributedTitle")
            alertController.addAction(alertAction)
            present(alertController, animated: true, completion: nil)
        }
    
    //View Tapped
    @IBAction func viewTapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    // Back action
    @IBAction func back(_ sender: UIBarButtonItem) {
        if isfromsignin
        {
            self.navigationController?.popToRootViewController(animated: true)
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
       
    }
    
}
extension BroucherDownloadVC:URLSessionDownloadDelegate
{
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        print("downloadLocation:", location)
            // create destination URL with the original pdf name
            guard let url = downloadTask.originalRequest?.url else { return }
            let documentsPath = FileManager.default.urls(for: .documentDirectory , in: .userDomainMask)[0]
            let destinationURL = documentsPath.appendingPathComponent(url.lastPathComponent)
            // delete original copy
            try? FileManager.default.removeItem(at: destinationURL)
            // copy from temp to Document
            do {
                try FileManager.default.copyItem(at: location, to: destinationURL)
                self.destinationpath = destinationURL
            } catch let error {
                print("Copy Error: \(error.localizedDescription)")
            }
         
        }
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        let progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
       
//        DispatchQueue.main.async {
//            self.progressDownloadIndicator.progress = progress
//        }
    }
    
}
extension BroucherDownloadVC:UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeTextField = textField
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.activeTextField = nil
    }
}
